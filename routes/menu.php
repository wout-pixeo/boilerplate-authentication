<?php

use App\Domain\Users\Enums\Role;
use Illuminate\Support\Facades\Auth;
use App\Domain\Modules\Models\Module;

$user = Auth::user();

if (! Auth::user()) {
    return [];
}

$menu = [
    'menu' => [],
    'add' => [],
];

data_set($menu, 'menu.dashboard', [
    'route' => 'dashboard.index',
    'icon' => 'far fa-tachometer-alt-average',
    'title' => 'Dashboard',
]);

if ($user->hasRole(Role::root()->getValue())) {
    data_set($menu, 'menu.root', [
        'title' => 'Root',
        'children' => [],
    ]);

    data_set($menu, 'menu.root.children.city', [
        'route' => 'root.cities.index',
        'icon' => 'far fa-city',
        'title' => 'Gemeenten',
    ]);

    data_set($menu, 'menu.root.children.domains', [
        'route' => 'root.domains.index',
        'icon' => 'far fa-link',
        'title' => 'Domeinen',
    ]);

    data_set($menu, 'menu.root.children.help', [
        'route' => 'root.helpItems.index',
        'icon' => 'fas fa-question-circle',
        'title' => 'Contextuele hulp',
    ]);

    data_set($menu, 'menu.root.children.users', [
        'route' => 'root.users.index',
        'icon' => 'far fa-users',
        'title' => 'Gebruikers',
    ]);
}

if ($user->hasRole(Role::admin()->getValue())) {
    $modules = Module::all()->each(function ($module) use (&$menu) {
        data_set($menu, 'menu.' . $module->slug, [
            'title' => $module->name,
            'module_id' => $module->id,
            'children' => [],
        ]);

        data_set($menu, 'menu.' . $module->slug.'.children.questions', [
            'route' => 'admin.questions.index',
            'params' => ['module' => $module->slug],
            'icon' => 'fas fa-question-circle',
            'title' => 'Vragen',
        ]);

        data_set($menu, 'menu.' . $module->slug.'.children.forms', [
            'route' => 'admin.forms.index',
            'params' => ['module' => $module->slug],
            'icon' => 'far fa-file',
            'title' => 'Formulieren',
        ]);

        data_set($menu, 'menu.' . $module->slug.'.children.question_forms', [
            'route' => 'admin.questionAnswers.index',
            'params' => ['module' => $module->slug],
            'icon' => 'far fa-link',
            'title' => 'Koppelingen',
        ]);

        data_set($menu, 'menu.' . $module->slug.'.children.items', [
            'route' => 'admin.moduleItems.index',
            'params' => ['module' => $module->slug],
            'icon' => 'far fa-layer-group',
            'title' => 'Dossiers',
            'shortcut' => true,
        ]);
    });

    data_set($menu, 'add.user', [
        'title' => 'Gebruiker',
        'route' => 'admin.users.create',
        'icon' => 'far fa-user',
    ]);

    data_set($menu, 'add.module', [
        'title' => 'Dossierstroom',
        'route' => 'admin.modules.create',
        'icon' => 'far fa-layer-group',
    ]);

    data_set($menu, 'menu.other', [
        'title' => 'Basisdata',
        'children' => [],
    ]);

    data_set($menu, 'menu.other.children.help', [
        'route' => 'admin.helpItems.index',
        'icon' => 'fas fa-question-circle',
        'title' => 'Contextuele hulp',
    ]);

    data_set($menu, 'menu.other.children.workflows', [
        'route' => 'admin.workflows.index',
        'icon' => 'far fa-layer-group',
        'title' => 'Workflows',
    ]);

    data_set($menu, 'menu.other.children.users', [
        'route' => 'admin.users.index',
        'icon' => 'far fa-users',
        'title' => 'Gebruikers',
    ]);
}
//                    $menu['events'] = [
//                        'title' => 'Evenementen',
//                        'children' => [
//                            'events' => [
//                                'route' => 'admin.events.index',
//                                'icon' => 'eva-gift-outline',
//                                'title' => 'Evenementen',
//                            ],
//                        ],
//                    ];
//
//                    $menu['clubs'] = [
//                        'title' => 'Verenigingen',
//                        'children' => [
//                            'clubs' => [
//                                'route' => 'admin.clubs.index',
//                                'icon' => 'eva-flag-outline',
//                                'title' => 'Verenigingen',
//                            ],
//                            'clubCategories' => [
//                                'route' => 'admin.clubCategories.index',
//                                'icon' => 'eva-pricetags-outline',
//                                'title' => 'Categoriën',
//                            ],
//                        ],
//                    ];
//
//                    $menu['other'] = [
//                        'title' => 'Basisdata',
//                        'children' => [
//                            'departments' => [
//                                'route' => 'admin.departments.index',
//                                'icon' => 'eva-bulb-outline',
//                                'title' => 'Diensten',
//                            ],
//                            'third_party' => [
//                                'route' => 'admin.thirdParties.index',
//                                'icon' => 'eva-radio-outline',
//                                'title' => 'Derde partijen',
//                            ],
//                            'locations' => [
//                                'route' => 'admin.locations.index',
//                                'icon' => 'eva-pin-outline',
//                                'title' => 'Locaties',
//                            ],
//                            'users' => [
//                                'route' => 'admin.users.index',
//                                'icon' => 'eva-people-outline',
//                                'title' => 'Gebruikers',
//                            ],
//                        ],
//                    ];
//
//                    $menu['procedures'] = [
//                        'title' => 'Procedures',
//                        'children' => [
//                            'questions' => [
//                                'route' => 'admin.questions.index',
//                                'icon' => 'eva-question-mark-outline',
//                                'title' => 'Basisvragen',
//                            ],
//                            'forms' => [
//                                'route' => 'admin.forms.index',
//                                'icon' => 'eva-clipboard-outline',
//                                'title' => 'Formulieren',
//                            ],
//                        ],
//                    ];
//                }
//
//
if ($user->hasRole(Role::user()->getValue())) {
    $modules = Module::all()->each(function ($module) use (&$menu) {
        data_set($menu, 'menu.' . $module->slug, [
            'route' => 'modules.index',
            'icon' => 'far fa-layer-group',
            'title' => $module->name,
            'params' => ['module' => $module->slug],
        ]);
    });
}

return $menu;
