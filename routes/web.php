<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Domain\Modules\Models\ModuleItem;
use App\Domain\Users\Enums\Role;
use App\Http\Controllers\Admin\AdvicesController;
use App\Http\Controllers\User\SubmitFormsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\AvatarController;
use App\Http\Controllers\Api\MediaController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Api\UploadController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Root\UsersController;
use App\Http\Controllers\Admin\ClubsController;
use App\Http\Controllers\Admin\FormsController;
use App\Http\Controllers\Root\CitiesController;
use App\Http\Controllers\Root\DomainsController;
use App\Http\Controllers\Admin\ModulesController;
use App\Http\Controllers\Api\ClubUsersController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Admin\QuestionsController;
use App\Http\Controllers\Admin\WorkflowsController;
use App\Http\Controllers\Api\DepartmentsController;
use App\Http\Controllers\Api\ThirdPartiesController;
use App\Http\Controllers\Root\PasswordUrlController;
use App\Http\Controllers\User\ModuleItemsController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Admin\OrderQuestionsController;
use App\Http\Controllers\Api\Location\StreetsController;
use App\Http\Controllers\Auth\CompleteAccountController;
use App\Http\Controllers\Admin\ModuleItemFormsController;
use App\Http\Controllers\Admin\QuestionAnswersController;
use App\Http\Controllers\Api\Location\PostInfoController;
use App\Http\Controllers\Api\Location\AddressesController;
use App\Http\Controllers\Api\Location\CountriesController;
use App\Http\Controllers\Root\SendPasswordResetController;
use App\Http\Controllers\Account\ImpersonateUserController;
use App\Http\Controllers\Admin\ModuleItemFormStatusController;
use App\Http\Controllers\Api\PostmarkSenderSignaturesController;
use App\Http\Controllers\Api\ClubsController as ApiClubsController;
use App\Http\Controllers\Api\Admin\CompletedWorkflowItemsController;
use App\Http\Controllers\User\FormsController as UserFormsController;
use App\Http\Controllers\Admin\UsersController as AdminUsersController;
use App\Http\Controllers\Api\UsersController as ApiUsersControllerAlias;
use App\Http\Controllers\Root\HelpItemsController as HelpItemsController;
use App\Http\Controllers\Admin\HelpItemsController as AdminHelpItemsController;
use App\Http\Controllers\Admin\ModuleItemsController as AdminModuleItemsController;

Route::get('/', [LoginController::class, 'showLoginForm'])
    ->name('home');

// Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login.attempt');
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

Route::get('register', [RegisterController::class, 'showRegistrationForm'])
    ->name('register');

Route::post('register', [RegisterController::class, 'register'])
    ->name('register.store');

// Password Reset Routes...
Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])
    ->name('password.request');
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])
    ->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])
    ->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class, 'reset'])
    ->name('password.reset.store');

Route::get('email/verify', [VerificationController::class, 'show'])
    ->name('verification.notice');
Route::get('email/verify/{id}/{hash}', [VerificationController::class, 'verify'])
    ->name('verification.verify');
Route::post('email/resend', [VerificationController::class, 'resend'])
    ->name('verification.resend');

Route::get('complete-account', [CompleteAccountController::class, 'show'])
    ->name('auth.complete');
Route::post('complete-account', [CompleteAccountController::class, 'store'])
    ->name('auth.complete.store');

Route::get('module-item/{id}', function (Request $request, $id) {
    $item = ModuleItem::find($id);

    // Redirect to correct controller
    if ($request->user()->hasRole(Role::admin()->getValue())) {
        return redirect()->route('admin.moduleItems.show', ['module' => $item->module->slug, 'id' => $id]);
    }

    return redirect('/');
})->name('mail.moduleItem.show');

Route::get('avatar/{name}', [AvatarController::class, 'index'])
    ->name('avatar');

// Localization
Route::get('/js/lang.js', function () {
    $strings = Cache::rememberForever('lang.js', function () {
        $lang = config('app.locale');

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

Route::group(['middleware' => ['auth', 'verified', 'complete']], function () {
    Route::get('/home', [DashboardController::class, 'index'])
        ->name('dashboard.index');

    Route::get('media/{slug}', [\App\Http\Controllers\MediaController::class, 'show'])
        ->name('media.show');

    Route::get('account', [AccountController::class, 'index'])
        ->name('account.index');

    Route::post('account', [AccountController::class, 'store'])
        ->name('account.store');

    /**
     * ROOT ROUTES
     */
    Route::group(['middleware' => ['role:' . Role::root()]], function () {
        Route::get('root/cities', [CitiesController::class, 'index'])
            ->name('root.cities.index')
            ->middleware('remember');
        Route::get('root/cities/create', [CitiesController::class, 'create'])
            ->name('root.cities.create');
        Route::get('root/cities/{id}/edit', [CitiesController::class, 'edit'])
            ->name('root.cities.edit');
        Route::post('cities', [CitiesController::class, 'store'])
            ->name('root.cities.store');
        Route::put('root/cities/{id}', [CitiesController::class, 'update'])
            ->name('root.cities.update');
        Route::delete('root/cities/{id}', [CitiesController::class, 'destroy'])
            ->name('root.cities.destroy');

        Route::get('root/domains', [DomainsController::class, 'index'])
            ->name('root.domains.index')
            ->middleware('remember');
        Route::get('root/domains/create', [DomainsController::class, 'create'])
            ->name('root.domains.create');
        Route::get('root/domains/{id}/edit', [DomainsController::class, 'edit'])
            ->name('root.domains.edit');
        Route::post('root/domains', [DomainsController::class, 'store'])
            ->name('root.domains.store');
        Route::put('root/domains/{id}', [DomainsController::class, 'update'])
            ->name('root.domains.update');
        Route::delete('root/domains/{id}', [DomainsController::class, 'destroy'])
            ->name('root.domains.destroy');

        Route::get('root/users', [UsersController::class, 'index'])
            ->name('root.users.index')
            ->middleware('remember');
        Route::get('root/users/create', [UsersController::class, 'create'])
            ->name('root.users.create');
        Route::get('root/users/{id}/edit', [UsersController::class, 'edit'])
            ->name('root.users.edit');
        Route::post('root/users', [UsersController::class, 'store'])
            ->name('root.users.store');
        Route::put('root/users/{id}', [UsersController::class, 'update'])
            ->name('root.users.update');
        Route::delete('root/users/{id}', [UsersController::class, 'destroy'])
            ->name('root.users.destroy');

        Route::post('users/{id}/send-reset-email', [SendPasswordResetController::class, 'store'])
            ->name('root.users.sendPassword');

        Route::post('users/{id}/reset-url', [PasswordUrlController::class, 'store'])
            ->name('root.users.resetUrl');

        Route::get('root/help-items', [HelpItemsController::class, 'index'])
            ->name('root.helpItems.index')
            ->middleware('remember');
        Route::get('root/help-items/create', [HelpItemsController::class, 'create'])
            ->name('root.helpItems.create');
        Route::get('root/help-items/{id}/edit', [HelpItemsController::class, 'edit'])
            ->name('root.helpItems.edit');
        Route::post('helpItems', [HelpItemsController::class, 'store'])
            ->name('root.helpItems.store');
        Route::put('root/help-items/{id}', [HelpItemsController::class, 'update'])
            ->name('root.helpItems.update');
        Route::delete('root/help-items/{id}', [HelpItemsController::class, 'destroy'])
            ->name('root.helpItems.destroy');
    });

    /**
     * ADMIN ROUTES
     */
    Route::group(['middleware' => ['role:' . Role::admin()]], function () {
        Route::get('admin/modules/create', [ModulesController::class, 'create'])
            ->name('admin.modules.create');
        Route::get('admin/modules/{id}/edit', [ModulesController::class, 'edit'])
            ->name('admin.modules.edit');
        Route::post('admin/modules', [ModulesController::class, 'store'])
            ->name('admin.modules.store');
        Route::put('admin/modules/{id}', [ModulesController::class, 'update'])
            ->name('admin.modules.update');
        Route::delete('admin/modules/{id}', [ModulesController::class, 'destroy'])
            ->name('admin.modules.destroy');

        Route::get('admin/users', [AdminUsersController::class, 'index'])
            ->name('admin.users.index')
            ->middleware('remember');
        Route::get('admin/users/create', [AdminUsersController::class, 'create'])
            ->name('admin.users.create');
        Route::get('admin/users/{id}/edit', [AdminUsersController::class, 'edit'])
            ->name('admin.users.edit');
        Route::post('admin/users', [AdminUsersController::class, 'store'])
            ->name('admin.users.store');
        Route::put('admin/users/{id}', [AdminUsersController::class, 'update'])
            ->name('admin.users.update');
        Route::delete('admin/users/{id}', [AdminUsersController::class, 'destroy'])
            ->name('admin.users.destroy');

        Route::get('admin/{module}/items', [AdminModuleItemsController::class, 'index'])
            ->name('admin.moduleItems.index');
        Route::get('admin/{module}/items/{id}', [AdminModuleItemsController::class, 'show'])
            ->name('admin.moduleItems.show');

        Route::get('admin/{module}/items/{itemId}/forms/{id}', [ModuleItemFormsController::class, 'show'])
            ->name('admin.moduleItemForms.show');

        Route::post('admin/{module}/items/{itemId}/forms/{id}/status', [ModuleItemFormStatusController::class, 'store'])
            ->name('admin.moduleItemFormStatus.store');

        Route::get('admin/{module}/questions/order', [OrderQuestionsController::class, 'index'])
            ->name('admin.questions.order.index');
        Route::put('admin/{module}/questions/order', [OrderQuestionsController::class, 'update'])
            ->name('admin.questions.order.update');

        Route::get('admin/{module}/questions', [QuestionsController::class, 'index'])
            ->name('admin.questions.index')
            ->middleware('remember');
        Route::get('admin/{module}/questions/create', [QuestionsController::class, 'create'])
            ->name('admin.questions.create');
        Route::get('admin/{module}/questions/{id}/edit', [QuestionsController::class, 'edit'])
            ->name('admin.questions.edit');
        Route::post('admin/{module}/questions', [QuestionsController::class, 'store'])
            ->name('admin.questions.store');
        Route::put('admin/{module}/questions/{id}', [QuestionsController::class, 'update'])
            ->name('admin.questions.update');
        Route::delete('admin/{module}/questions/{id}', [QuestionsController::class, 'destroy'])
            ->name('admin.questions.destroy');

        Route::get('admin/{module}/questions-answers', [QuestionAnswersController::class, 'index'])
            ->name('admin.questionAnswers.index');
        Route::post('admin/{module}/questions-answers', [QuestionAnswersController::class, 'store'])
            ->name('admin.questionAnswers.store');

        Route::get('admin/{module}/forms', [FormsController::class, 'index'])
            ->name('admin.forms.index');
        Route::get('admin/{module}/forms/create', [FormsController::class, 'create'])
            ->name('admin.forms.create');
        Route::get('admin/{module}/forms/{id}/edit', [FormsController::class, 'edit'])
            ->name('admin.forms.edit');
        Route::post('admin/{module}/forms', [FormsController::class, 'store'])
            ->name('admin.forms.store');
        Route::put('admin/{module}/forms/{id}', [FormsController::class, 'update'])
            ->name('admin.forms.update');
        Route::delete('admin/{module}/forms/{id}', [FormsController::class, 'destroy'])
            ->name('admin.forms.destroy');

        Route::get('admin/help-items', [AdminHelpItemsController::class, 'index'])
            ->name('admin.helpItems.index')
            ->middleware('remember');
        Route::get('admin/help-items/{id}/edit', [AdminHelpItemsController::class, 'edit'])
            ->name('admin.helpItems.edit');
        Route::put('admin/help-items/{id}', [AdminHelpItemsController::class, 'update'])
            ->name('admin.helpItems.update');
        Route::delete('admin/help-items/{id}', [AdminHelpItemsController::class, 'destroy'])
            ->name('admin.helpItems.destroy');

        Route::get('admin/workflows', [WorkflowsController::class, 'index'])
            ->name('admin.workflows.index');
        Route::get('admin/workflows/create', [WorkflowsController::class, 'create'])
            ->name('admin.workflows.create');
        Route::get('admin/workflows/{id}/edit', [WorkflowsController::class, 'edit'])
            ->name('admin.workflows.edit');
        Route::post('admin/workflows', [WorkflowsController::class, 'store'])
            ->name('admin.workflows.store');
        Route::put('admin/workflows/{id}', [WorkflowsController::class, 'update'])
            ->name('admin.workflows.update');
        Route::delete('admin/workflows/{id}', [WorkflowsController::class, 'destroy'])
            ->name('admin.workflows.destroy');

        Route::get('admin/{module}/items/{itemId}/forms/{id}/advices/create', [AdvicesController::class, 'create'])
            ->name('admin.advices.create');
        Route::post('admin/{module}/items/{itemId}/forms/{id}/advices', [AdvicesController::class, 'store'])
            ->name('admin.advices.store');

//        Route::get('admin/clubs', [ClubsController::class, 'index'])
//            ->name('admin.clubs.index')
//            ->middleware('remember');
//        Route::get('admin/clubs/create', [ClubsController::class, 'create'])
//            ->name('admin.clubs.create');
//        Route::get('admin/clubs/{id}/edit', [ClubsController::class, 'edit'])
//            ->name('admin.clubs.edit');
//        Route::post('admin/clubs', [ClubsController::class, 'store'])
//            ->name('admin.clubs.store');
//        Route::put('admin/clubs/{id}', [ClubsController::class, 'update'])
//            ->name('admin.clubs.update');
//        Route::delete('admin/clubs/{id}', [ClubsController::class, 'destroy'])
//            ->name('admin.clubs.destroy');
//
//        Route::post('admin/clubs/{id}/approve', [ClubsApproveController::class, 'store'])
//            ->name('admin.clubs.approve');
//        Route::delete('admin/clubs/{id}/disapprove', [ClubsApproveController::class, 'destroy'])
//            ->name('admin.clubs.disapprove');
//
//        Route::get('admin/club-categories', [ClubCategoriesController::class, 'index'])
//            ->name('admin.clubCategories.index');
//        Route::get('admin/club-categories/create', [ClubCategoriesController::class, 'create'])
//            ->name('admin.clubCategories.create');
//        Route::get('admin/club-categories/{id}/edit', [ClubCategoriesController::class, 'edit'])
//            ->name('admin.clubCategories.edit');
//        Route::post('admin/club-categories', [ClubCategoriesController::class, 'store'])
//            ->name('admin.clubCategories.store');
//        Route::put('admin/club-categories/{id}', [ClubCategoriesController::class, 'update'])
//            ->name('admin.clubCategories.update');
//        Route::delete('admin/club-categories/{id}', [ClubCategoriesController::class, 'destroy'])
//            ->name('admin.clubCategories.destroy');
//
//        Route::get('admin/third-parties', [ThirdPartiesController::class, 'index'])
//            ->name('admin.thirdParties.index');
//        Route::get('admin/third-parties/create', [ThirdPartiesController::class, 'create'])
//            ->name('admin.thirdParties.create');
//        Route::get('admin/third-parties/{id}/edit', [ThirdPartiesController::class, 'edit'])
//            ->name('admin.thirdParties.edit');
//        Route::post('admin/third-parties', [ThirdPartiesController::class, 'store'])
//            ->name('admin.thirdParties.store');
//        Route::put('admin/third-parties/{id}', [ThirdPartiesController::class, 'update'])
//            ->name('admin.thirdParties.update');
//        Route::delete('admin/third-parties/{id}', [ThirdPartiesController::class, 'destroy'])
//            ->name('admin.thirdParties.destroy');
//
//        Route::get('admin/departments', [DepartmentsController::class, 'index'])
//            ->name('admin.departments.index');
//        Route::get('admin/departments/create', [DepartmentsController::class, 'create'])
//            ->name('admin.departments.create');
//        Route::get('admin/departments/{id}/edit', [DepartmentsController::class, 'edit'])
//            ->name('admin.departments.edit');
//        Route::post('admin/departments', [DepartmentsController::class, 'store'])
//            ->name('admin.departments.store');
//        Route::put('admin/departments/{id}', [DepartmentsController::class, 'update'])
//            ->name('admin.departments.update');
//        Route::delete('admin/departments/{id}', [DepartmentsController::class, 'destroy'])
//            ->name('admin.departments.destroy');
//
//        Route::get('admin/locations', [LocationsController::class, 'index'])
//            ->name('admin.locations.index');
//        Route::get('admin/locations/create', [LocationsController::class, 'create'])
//            ->name('admin.locations.create');
//        Route::get('admin/locations/{id}/edit', [LocationsController::class, 'edit'])
//            ->name('admin.locations.edit');
//        Route::post('admin/locations', [LocationsController::class, 'store'])
//            ->name('admin.locations.store');
//        Route::put('admin/locations/{id}', [LocationsController::class, 'update'])
//            ->name('admin.locations.update');
//        Route::delete('admin/locations/{id}', [LocationsController::class, 'destroy'])
//            ->name('admin.locations.destroy');
//

//        Route::get('admin/events', [AdminEventsControllerAlias::class, 'index'])
//            ->name('admin.events.index');
//        Route::get('admin/events/{id}', [AdminEventsControllerAlias::class, 'show'])
//            ->name('admin.events.show');
//        Route::delete('admin/events/{id}', [AdminEventsControllerAlias::class, 'destroy'])
//            ->name('admin.events.destroy');
    });

    /**
     * USER ROUTES
     */
    Route::group(['middleware' => ['role:' . Role::user()]], function () {
        Route::get('modules/{module}', [ModuleItemsController::class, 'index'])
            ->name('modules.index');
        Route::get('modules/{module}/create', [ModuleItemsController::class, 'create'])
            ->name('modules.create');
        Route::get('modules/{module}/{id}', [ModuleItemsController::class, 'show'])
            ->name('modules.show');
        Route::post('modules/{module}', [ModuleItemsController::class, 'store'])
            ->name('modules.store');

        Route::get('forms/{moduleItemId}/{id}', [UserFormsController::class, 'show'])
            ->name('forms.show');
        Route::get('forms/{moduleItemId}/{id}/edit', [UserFormsController::class, 'edit'])
            ->name('forms.edit');
        Route::put('forms/{moduleItemId}/{id}/', [UserFormsController::class, 'update'])
            ->name('forms.update');

        Route::post('forms/{moduleItemId}/{id}', [SubmitFormsController::class, 'store'])
            ->name('submitForms.store');

        ////        Route::get('events/{id}/edit', [OrganiserEventsController::class, 'edit'])
////            ->name('user.events.edit');
////        Route::get('events/{id}', [OrganiserEventsController::class, 'show'])
////            ->name('user.events.show');
////        Route::post('events', [OrganiserEventsController::class, 'store'])
////            ->name('user.events.store');
////        Route::put('events/{id}', [OrganiserEventsController::class, 'update'])
////            ->name('user.events.update');
////        Route::delete('events/{id}', [OrganiserEventsController::class, 'destroy'])
////            ->name('user.events.destroy');
    });

    // User Impersonation
    Route::group(['middleware' => ['role:' . Role::root() . '|' . Role::admin()]], function () {
        Route::put('impersonate-user/{id}', [ImpersonateUserController::class, 'update'])
            ->name('impersonate.update');
    });

    Route::delete('impersonate-user', [ImpersonateUserController::class, 'destroy'])
        ->name('impersonate.destroy');

    Route::prefix('api')
        ->group(function () {
            Route::post('upload', [UploadController::class, 'store'])
                ->name('api.upload.store');

            Route::get('media/{id}', [MediaController::class, 'show'])
                ->name('api.media.show');

            Route::get('clubs', [ApiClubsController::class, 'index'])
                ->name('api.clubs.index');

            Route::get('users', [ApiUsersControllerAlias::class, 'index'])
                ->name('api.users.index');

            Route::get('forms', [\App\Http\Controllers\Api\FormsController::class, 'index'])
                ->name('api.forms.index');

            Route::post('club-users', [ClubUsersController::class, 'store'])
                ->name('api.clubUsers.store');
            Route::delete('club-users', [ClubUsersController::class, 'destroy'])
                ->name('api.clubUsers.destroy');

            Route::get('third-parties', [ThirdPartiesController::class, 'index'])
                ->name('api.thirdParties.index');

            Route::get('departments', [DepartmentsController::class, 'index'])
                ->name('api.departments.index');

            Route::get('locations/countries', [CountriesController::class, 'index'])
                ->name('api.locations.countries.index');

            Route::get('locations/cities', [\App\Http\Controllers\Api\Location\CitiesController::class, 'index'])
                ->name('api.locations.cities.index');

            Route::get('locations/streets', [StreetsController::class, 'index'])
                ->name('api.locations.streets.index');

            Route::get('locations/addresses', [AddressesController::class, 'index'])
                ->name('api.locations.addresses.index');

            Route::get('locations/postinfo', [PostInfoController::class, 'index'])
                ->name('api.locations.postinfo.index');

            Route::get('sender-signatures', [PostmarkSenderSignaturesController::class, 'index'])
                ->name('api.postmark.index')
                ->middleware('role:' . Role::root());

            Route::get('sender-signatures/{id}', [PostmarkSenderSignaturesController::class, 'show'])
                ->name('api.postmark.show')
                ->middleware('role:' . Role::root());

            Route::post('completed-workflow-items', [CompletedWorkflowItemsController::class, 'store'])
                ->name('api.admin.completedWorkflowItems.store')
                ->middleware('role:' . Role::admin());
            Route::delete('completed-workflow-items/{id}', [CompletedWorkflowItemsController::class, 'destroy'])
                ->name('api.admin.completedWorkflowItems.destroy')
                ->middleware('role:' . Role::admin());
        });
});
