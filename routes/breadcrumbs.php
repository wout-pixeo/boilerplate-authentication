<?php

// Home

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Aanmelden', route('home'));
});

Breadcrumbs::for('login', function ($trail) {
    $trail->push('Aanmelden', route('home'));
});

// Password reset
Breadcrumbs::for('password.request', function ($trail) {
    $trail->push('Wachtwoord vergeten', route('home'));
});

Breadcrumbs::for('password.reset', function ($trail) {
    $trail->push('Wachtwoord vergeten', route('home'));
});

// Register
Breadcrumbs::for('register', function ($trail) {
    $trail->push('Registreren', route('home'));
});

Breadcrumbs::for('verification.notice', function ($trail) {
    $trail->push('E-mail bevestigen', route('home'));
});

Breadcrumbs::for('auth.complete', function ($trail) {
    $trail->push('Account vervolledigen', route('home'));
});

// Account
Breadcrumbs::for('account.index', function ($trail) {
    $trail->push('Profiel', route('account.index'));
});

// Dashboard
Breadcrumbs::for('dashboard.index', function ($trail) {
    $trail->push('Dashboard', route('dashboard.index'));
});

// Root > Cities
Breadcrumbs::for('root.cities.index', function ($trail) {
    $trail->push('Gemeenten', route('root.cities.index'));
});

Breadcrumbs::for('root.cities.create', function ($trail) {
    $trail->parent('root.cities.index');
    $trail->push('Gemeente toevoegen', route('root.cities.create'));
});

Breadcrumbs::for('root.cities.edit', function ($trail, $id) {
    $trail->parent('root.cities.index');
    $trail->push('Gemeente bewerken', route('root.cities.edit', $id));
});

// Root > Domains
Breadcrumbs::for('root.domains.index', function ($trail) {
    $trail->push('Domeinen', route('root.domains.index'));
});

Breadcrumbs::for('root.domains.create', function ($trail) {
    $trail->parent('root.domains.index');
    $trail->push('Domein toevoegen', route('root.domains.create'));
});

Breadcrumbs::for('root.domains.edit', function ($trail, $id) {
    $trail->parent('root.domains.index');
    $trail->push('Domein bewerken', route('root.domains.edit', $id));
});

// Root > Users
Breadcrumbs::for('root.users.index', function ($trail) {
    $trail->push('Gebruikers', route('root.users.index'));
});

Breadcrumbs::for('root.users.create', function ($trail) {
    $trail->parent('root.users.index');
    $trail->push('Gebruiker toevoegen', route('root.users.create'));
});

Breadcrumbs::for('root.users.edit', function ($trail, $id) {
    $trail->parent('root.users.index');
    $trail->push('Gebruiker bewerken', route('root.users.edit', $id));
});

// Root > Help Items
Breadcrumbs::for('root.helpItems.index', function ($trail) {
    $trail->push('Contextuele hulp', route('root.helpItems.index'));
});

Breadcrumbs::for('root.helpItems.create', function ($trail) {
    $trail->parent('root.helpItems.index');
    $trail->push('Hulpitem toevoegen', route('root.helpItems.create'));
});

Breadcrumbs::for('root.helpItems.edit', function ($trail, $id) {
    $trail->parent('root.helpItems.index');
    $trail->push('Hulpitem bewerken', route('root.helpItems.edit', $id));
});

// Admin > Modules
Breadcrumbs::for('admin.modules.create', function ($trail) {
    $trail->push('Dossierstroom toevoegen', route('admin.modules.create'));
});

Breadcrumbs::for('admin.modules.edit', function ($trail) {
    $trail->push('Dossierstroom bewerken');
});

// Admin > Users
Breadcrumbs::for('admin.users.index', function ($trail) {
    $trail->push('Gebruikers', route('admin.users.index'));
});

Breadcrumbs::for('admin.users.create', function ($trail) {
    $trail->parent('admin.users.index');
    $trail->push('Gebruiker toevoegen', route('admin.users.create'));
});

Breadcrumbs::for('admin.users.edit', function ($trail, $id) {
    $trail->parent('admin.users.index');
    $trail->push('Gebruiker bewerken', route('admin.users.edit', $id));
});

// Admin > Questions
Breadcrumbs::for('admin.questions.index', function ($trail, $module) {
    $trail->push('Vragen', route('admin.questions.index', ['module' => $module]));
});

Breadcrumbs::for('admin.questions.create', function ($trail, $module) {
    $trail->parent('admin.questions.index', $module);
    $trail->push('Vraag toevoegen', route('admin.questions.create', ['module' => $module]));
});

Breadcrumbs::for('admin.questions.edit', function ($trail, $module, $id) {
    $trail->parent('admin.questions.index', $module);
    $trail->push('Vraag bewerken', route('admin.questions.edit', ['module' => $module, 'id' => $id]));
});

Breadcrumbs::for('admin.questions.order.index', function ($trail, $module) {
    $trail->parent('admin.questions.index', $module);
    $trail->push('Volgorde vragen beheren', route('admin.questions.order.index', ['module' => $module]));
});

Breadcrumbs::for('admin.questionAnswers.index', function ($trail, $module) {
    $trail->parent('admin.questions.index', $module);
    $trail->push("Koppelingen beheren", route('admin.questionAnswers.index', ['module' => $module]));
});

// Admin > Forms
Breadcrumbs::for('admin.forms.index', function ($trail, $module) {
    $trail->push('Formulieren', route('admin.forms.index', ['module' => $module]));
});

Breadcrumbs::for('admin.forms.create', function ($trail, $module) {
    $trail->parent('admin.forms.index', $module);
    $trail->push('Formulier toevoegen', route('admin.forms.create', ['module' => $module]));
});

Breadcrumbs::for('admin.forms.edit', function ($trail, $module, $id) {
    $trail->parent('admin.forms.index', $module);
    $trail->push('Formulier bewerken', route('admin.forms.edit', ['module' => $module, 'id' => $id]));
});

// Admin > Module Items
Breadcrumbs::for('admin.moduleItems.index', function ($trail, $slug) {
    $model = \App\Domain\Modules\Models\Module::findBySlugOrFail($slug);
    $trail->push($model->name, route('admin.moduleItems.index', ['module' => $slug]));
});

Breadcrumbs::for('admin.moduleItems.show', function ($trail, $slug, $id) {
    $trail->parent('admin.moduleItems.index', $slug);
    $trail->push($id, route('admin.moduleItems.show', ['module' => $slug, 'id' => $id]));
});

// Admin > Module Items > Form
Breadcrumbs::for('admin.moduleItemForms.show', function ($trail, $slug, $itemId, $id) {
    $form = \App\Domain\Modules\Models\Form::find($id);
    $trail->parent('admin.moduleItems.show', $slug, $itemId);
    $trail->push($form->name, route('admin.moduleItemForms.show', ['module' => $slug, 'itemId' => $itemId, 'id' => $id]));
});

// Admin > Module Items > Form > Advices
Breadcrumbs::for('admin.advices.create', function ($trail, $slug, $itemId, $id) {
    $trail->parent('admin.moduleItemForms.show', $slug, $itemId, $id);
    $trail->push('Advies toevoegen', route('admin.advices.create', ['module' => $slug, 'itemId' => $itemId, 'id' => $id]));
});

// Admin > Help Items
Breadcrumbs::for('admin.helpItems.index', function ($trail) {
    $trail->push('Contextuele hulp', route('admin.helpItems.index'));
});

Breadcrumbs::for('admin.helpItems.create', function ($trail) {
    $trail->parent('admin.helpItems.index');
    $trail->push('Hulpitem toevoegen', route('admin.helpItems.create'));
});

Breadcrumbs::for('admin.helpItems.edit', function ($trail, $id) {
    $trail->parent('admin.helpItems.index');
    $trail->push('Hulpitem bewerken', route('admin.helpItems.edit', $id));
});

// Admin > Users
Breadcrumbs::for('admin.workflows.index', function ($trail) {
    $trail->push('Workflows', route('admin.workflows.index'));
});

Breadcrumbs::for('admin.workflows.create', function ($trail) {
    $trail->parent('admin.workflows.index');
    $trail->push('Workflow toevoegen', route('admin.workflows.create'));
});

Breadcrumbs::for('admin.workflows.edit', function ($trail, $id) {
    $trail->parent('admin.workflows.index');
    $trail->push('Workflow bewerken', route('admin.workflows.edit', $id));
});


//// Admin > Clubs
//Breadcrumbs::for('admin.clubs.index', function ($trail) {
//    $trail->push('Verenigingen', route('admin.clubs.index'));
//});
//
//Breadcrumbs::for('admin.clubs.create', function ($trail) {
//    $trail->parent('admin.clubs.index');
//    $trail->push('Vereniging toevoegen', route('admin.clubs.create'));
//});
//
//Breadcrumbs::for('admin.clubs.edit', function ($trail, $id) {
//    $trail->parent('admin.clubs.index');
//    $trail->push('Vereniging bewerken', route('admin.clubs.edit', $id));
//});
//
//// Admin > Club categories
//Breadcrumbs::for('admin.clubCategories.index', function ($trail) {
//    $trail->parent('admin.clubs.index');
//    $trail->push('Categorieën', route('admin.clubCategories.index'));
//});
//
//Breadcrumbs::for('admin.clubCategories.create', function ($trail) {
//    $trail->parent('admin.clubCategories.index');
//    $trail->push('Categorie toevoegen', route('admin.clubCategories.create'));
//});
//
//Breadcrumbs::for('admin.clubCategories.edit', function ($trail, $id) {
//    $trail->parent('admin.clubCategories.index');
//    $trail->push('Categorie bewerken', route('admin.clubCategories.edit', $id));
//});
//
//// Admin > Club categories
//Breadcrumbs::for('admin.departments.index', function ($trail) {
//    $trail->push('Dienst', route('admin.departments.index'));
//});
//
//Breadcrumbs::for('admin.departments.create', function ($trail) {
//    $trail->parent('admin.departments.index');
//    $trail->push('Dienst toevoegen', route('admin.departments.create'));
//});
//
//Breadcrumbs::for('admin.departments.edit', function ($trail, $id) {
//    $trail->parent('admin.departments.index');
//    $trail->push('Dienst bewerken', route('admin.departments.edit', $id));
//});
//
//// Admin > Third parties
//Breadcrumbs::for('admin.thirdParties.index', function ($trail) {
//    $trail->push('Derde partijen', route('admin.thirdParties.index'));
//});
//
//Breadcrumbs::for('admin.thirdParties.create', function ($trail) {
//    $trail->parent('admin.thirdParties.index');
//    $trail->push('Derde partij toevoegen', route('admin.thirdParties.create'));
//});
//
//Breadcrumbs::for('admin.thirdParties.edit', function ($trail, $id) {
//    $trail->parent('admin.thirdParties.index');
//    $trail->push('Derde partij bewerken', route('admin.thirdParties.edit', $id));
//});
//
//// Admin > Locations
//Breadcrumbs::for('admin.locations.index', function ($trail) {
//    $trail->push('Locaties', route('admin.locations.index'));
//});
//
//Breadcrumbs::for('admin.locations.create', function ($trail) {
//    $trail->parent('admin.locations.index');
//    $trail->push('Locatie toevoegen', route('admin.locations.create'));
//});
//
//Breadcrumbs::for('admin.locations.edit', function ($trail, $id) {
//    $trail->parent('admin.locations.index');
//    $trail->push('Locatie bewerken', route('admin.locations.edit', $id));
//});
//
//// Admin > Events
//Breadcrumbs::for('admin.events.index', function ($trail) {
//    $trail->parent('admin.events.index');
//    $trail->push('Evenementen', route('admin.events.index'));
//});
// Users > Modules
Breadcrumbs::for('modules.index', function ($trail, $slug) {
    $model = \App\Domain\Modules\Models\Module::findBySlugOrFail($slug);
    $trail->push($model->name, route('modules.index', ['module' => $slug]));
});

Breadcrumbs::for('modules.create', function ($trail, $module) {
    $trail->parent('modules.index', $module);
    $trail->push('Nieuwe aanvraag toevoegen', route('modules.create', ['module' => $module]));
});

Breadcrumbs::for('modules.show', function ($trail, $slug, $id) {
    $trail->parent('modules.index', $slug);
    $trail->push($id, route('modules.show', ['module' => $slug, 'id' => $id]));
});

Breadcrumbs::for('forms.show', function ($trail, $moduleItemId, $id) {
    $form = \App\Domain\Modules\Models\Form::find($id);
    $moduleItem = \App\Domain\Modules\Models\ModuleItem::find($moduleItemId);

    $trail->parent('modules.show', $form->module->slug, $moduleItem->id);
    $trail->push($form->name, route('forms.show', ['id' => $id, 'moduleItemId' => $moduleItemId]));
});

Breadcrumbs::for('forms.edit', function ($trail, $moduleItemId, $id) {
    $form = \App\Domain\Modules\Models\Form::find($id);
    $moduleItem = \App\Domain\Modules\Models\ModuleItem::find($moduleItemId);

    $trail->parent('modules.show', $form->module->slug, $moduleItem->id);
    $trail->push($form->name, route('forms.edit', ['id' => $id, 'moduleItemId' => $moduleItemId]));
});
