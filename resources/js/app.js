import { InertiaApp } from '@inertiajs/inertia-vue';
import PortalVue from 'portal-vue';
import Vue from 'vue';
import VueMeta from 'vue-meta';
import VTooltip from 'v-tooltip';
import VueWait from 'vue-wait';
import VModal from 'vue-js-modal/dist/ssr.nocss';
import AsyncComputed from 'vue-async-computed';
import axios from 'axios';
import get from 'lodash/get';
import Base from './Shared/Mixins/Base';
import Store from './Store/index';

Vue.config.productionTip = false;

Vue.mixin({ methods: { route: window.route } });
Vue.mixin(Base);

Vue.use(InertiaApp);
Vue.use(PortalVue);
Vue.use(VModal, { dynamic: true });
Vue.use(AsyncComputed);
Vue.use(VTooltip);
Vue.use(VueWait);
Vue.use(VueMeta);

// support passing down scopedSlots with v-bind
// https://github.com/vuejs/vue/pull/7765#issuecomment-401985464
// eslint-disable-next-line no-underscore-dangle,func-names
Vue.prototype._b = (function (bind) {
    // eslint-disable-next-line
    return function (data, tag, value, asProp, isSync) {
        if (value && value.$scopedSlots) {
            data.scopedSlots = value.$scopedSlots;
            delete value.$scopedSlots;
        }
        // eslint-disable-next-line prefer-rest-params
        return bind.apply(this, arguments);
    };
// eslint-disable-next-line no-underscore-dangle
}(Vue.prototype._b));

const init = () => {
    Vue.prototype.$t = (string) => get(window.i18n, string);

    // eslint-disable-next-line no-undef
    axios.defaults.withCredentials = true;

    Vue.prototype.$http = axios;
    Vue.prototype.$store = Store;

    /**
     * Next, we will create a fresh Vue application instance and attach it to
     * the page. Then, you may begin adding components to this application
     * or customize the JavaScript scaffolding to fit your unique needs.
     */
    const app = document.getElementById('app');

    new Vue({
        metaInfo: {
            title: 'Loading…',
            titleTemplate: '%s | Flowlab',
        },

        wait: new VueWait(),

        render: (h) => h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                // eslint-disable-next-line global-require,import/no-dynamic-require
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
    }).$mount(app);
};

init();
