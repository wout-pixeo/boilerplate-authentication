import last from 'lodash/last';
import Layout from '../../Layouts/Layout.vue';

export default (component) => ({
    metaInfo() {
        return {
            title: last(this.$page.breadcrumbs).title,
        };
    },

    layout: Layout,
    ...component,
});
