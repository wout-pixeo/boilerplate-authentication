import mapValues from 'lodash/mapValues';
import throttle from 'lodash/throttle';
import pickBy from 'lodash/pickBy';

export default (indexRoute, tableActions) => ({
    props: {
        models: Object,
        filters: Object,
    },

    data() {
        return {
            form: {
                search: this.filters.search,
                order_by: this.filters.order_by || 'name',
                order_by_dir: this.filters.order_by_dir || 'asc',
                trashed: this.filters.trashed,
                status: this.filters.status,
            },

            tableActions: [
                {
                    action: 'edit',
                    title: 'Bewerken',
                },
                {
                    divided: true,
                    action: 'delete',
                    title: 'Verwijderen...',
                },
            ],

            formWatcher: null,
        };
    },

    created() {
        this.formWatcher = this.$watch('form', throttle(() => {
            const query = pickBy(this.form);
            this.$inertia.replace(this.route(indexRoute, Object.keys(query).length ? query : { remember: 'forget' }));
        }, 150), { deep: true });
    },

    methods: {
        reset() {
            this.form = mapValues(this.form, () => null);
        },

        setOrdering({ id, order }) {
            this.$set(this.form, 'order_by', id);
            this.$set(this.form, 'order_by_dir', order);
        },

        handleAction({ action, entry }) {
            tableActions[action].apply(this, [entry]);
        },
    },
});
