import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import formatRelative from 'date-fns/formatRelative';
import formatDistance from 'date-fns/formatDistance';
import { nl } from 'date-fns/locale';
import truncate from 'lodash/truncate';
import get from 'lodash/get';

export default {
    computed: {
        uid() {
            // eslint-disable-next-line no-underscore-dangle
            return this._uid;
        },
    },

    methods: {
        async submit() {
            this.sending = true;

            const [method, ...params] = this.submitData();

            await this.$inertia[method](...params);

            this.sending = false;
        },

        confirmDelete(title, text, route, buttonText = 'Verwijderen') {
            this.$modal.show('dialog', {
                title,
                text,
                buttons: [
                    {
                        title: 'Annuleren',
                        class: 'btn-secondary',
                    },
                    {
                        title: buttonText,
                        class: 'btn-danger',
                        handler: () => {
                            this.$inertia.delete(route);
                            this.$modal.hide('dialog');
                        },
                    },
                ],
            });
        },

        confirm(title, text, callback, buttonText = 'Ok') {
            this.$modal.show('dialog', {
                title,
                text,
                buttons: [
                    {
                        title: 'Annuleren',
                        class: 'btn-secondary',
                    },
                    {
                        title: buttonText,
                        class: 'btn-primary',
                        handler: () => {
                            callback();
                            this.$modal.hide('dialog');
                        },
                    },
                ],
            });
        },

        getError(key) {
            return get(this.$page.errors, key);
        },
    },

    filters: {
        date(value, formatString = 'dd/MM/yyyy') {
            return format(parseISO(value), formatString, {
                locale: nl,
            });
        },

        formatRelative(value) {
            return formatRelative(parseISO(value), new Date(), {
                locale: nl,
                weekStartsOn: 1,
            });
        },

        formatDistance(value) {
            return formatDistance(parseISO(value), new Date(), {
                locale: nl,
                weekStartsOn: 1,
            });
        },

        /**
         * Truncate the given string.
         */
        truncate(string, length = 70) {
            return truncate(string, { length, separator: /,? +/ });
        },
    },
};
