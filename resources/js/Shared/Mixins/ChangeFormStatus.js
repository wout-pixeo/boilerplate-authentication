import StatusModal from '../DataEntry/StatusModal/StatusModal.vue';

export default {
    methods: {
        changeStatus(form, module, moduleItemId) {
            let status = null;

            this.$modal.show('dialog', {
                title: `Status formulier “${form.name}” wijzigen`,
                component: StatusModal,
                width: 800,
                props: {
                    allowedStatusses: [
                        this.$store.state.statusses.FORM.APPROVED,
                        this.$store.state.statusses.FORM.CONDITIONALAPPROVED,
                        this.$store.state.statusses.FORM.DISAPPROVED,
                    ],
                    value: form.status,
                    type: 'FORM',
                },
                events: {
                    input: (value) => {
                        status = value;
                    },
                },
                buttons: [
                    {
                        title: 'Annuleren',
                        class: 'btn-secondary',
                    },
                    {
                        title: 'Status wijzigen',
                        class: 'btn-primary',
                        handler: (i, event, source, dialog) => {
                            const validated = dialog.$refs.component.validate();

                            if (validated) {
                                // doe hier verder
                                // de input is gevalideerd.
                                this.$inertia.post(this.route('admin.moduleItemFormStatus.store', {
                                    itemId: moduleItemId,
                                    module,
                                    id: form.id,
                                }), {
                                    status,
                                });

                                this.$modal.hide('dialog');
                            }
                        },
                    },
                ],
            });
        },
    },
};
