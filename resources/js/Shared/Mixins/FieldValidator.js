// eslint-disable-next-line max-classes-per-file
class RequiredRule {
    static isValid(value) {
        return value;
    }
}

class EmailRule {
    static isValid(value) {
        const regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

        return regex.test(value);
    }
}

export default {
    data() {
        return {
            errors: {},
        };
    },

    methods: {
        validateField(key, value, rules = ['required']) {
            const errors = [];
            rules.forEach((rule) => {
                if (rule === 'required') {
                    if (!RequiredRule.isValid(value)) {
                        errors.push('Verplicht');
                    }
                } else if (rule === 'email') {
                    if (!EmailRule.isValid(value)) {
                        errors.push('Voeg een e-mailadres toe in volgend formaat: jouwnaam@voorbeeld.be');
                    }
                }
            });

            if (errors.length > 0) {
                this.$set(this.errors, key, errors);
            } else {
                this.$delete(this.errors, key);
            }
        },
    },
};
