import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Model from '@ckeditor/ckeditor5-ui/src/model';
import Collection from '@ckeditor/ckeditor5-utils/src/collection';
import {
    addListToDropdown,
    createDropdown,
} from '@ckeditor/ckeditor5-ui/src/dropdown/utils';

export default class VariablesDropdown extends Plugin {
    init() {
        const { editor } = this;

        editor.ui.componentFactory.add('variablesDropdown', (locale) => {
            const dropdownView = createDropdown(locale);

            dropdownView.buttonView.set({
                label: 'Dynamische velden',
                withText: true,
            });

            const variables = editor.config.get('variablesDropdown');

            const items = new Collection();

            variables.forEach((variable) => {
                items.add({
                    type: 'button',
                    model: new Model({
                        withText: true,
                        label: variable.title,
                        value: variable.id,
                    }),
                });
            });

            // Callback executed once the image is clicked.
            dropdownView.on('execute', ({ source }) => {
                editor.model.change((writer) => {
                    const insertPosition = editor.model.document.selection.getFirstPosition();
                    writer.insertText(source.value, insertPosition);
                });
            });

            addListToDropdown(dropdownView, items);

            return dropdownView;
        });
    }
}
