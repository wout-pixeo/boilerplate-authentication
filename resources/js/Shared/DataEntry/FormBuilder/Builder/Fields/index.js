import File from './File.vue';
import Options from './Options.vue';
import TextArea from './TextArea.vue';
import Date from './Date.vue';
import DateTime from './DateTime.vue';
import Download from './Download.vue';
import Title from './Title.vue';
import Text from './Text.vue';
import Number from './Number.vue';
import Boolean from './Boolean.vue';
import TextInput from './TextInput.vue';
import Location from './Location.vue';

export default {
    File,
    Options,
    TextArea,
    Date,
    DateTime,
    Download,
    Title,
    Text,
    Number,
    Boolean,
    TextInput,
    Location,
};
