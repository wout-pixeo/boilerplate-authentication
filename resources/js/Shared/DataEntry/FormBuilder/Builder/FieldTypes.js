/* eslint-disable import/prefer-default-export,object-curly-newline */
const VALUES = {
    TITLE: 'title',
    TEXT: 'text',
    INPUT: 'input',
    TEXTAREA: 'textarea',
    NUMBER: 'number',
    DATE: 'date',
    DATETIME: 'datetime',
    BOOLEAN: 'boolean',
    CHECKBOX: 'checkbox',
    RADIO: 'radio',
    SELECT: 'select',
    FILE: 'file',
    LATLNG: 'latlng',
    DOWNLOAD: 'download',
    LINK: 'link',
};

const FIELD_TYPES = [
    {
        id: VALUES.TITLE,
        component: 'Title',
        title: 'Titel',
        validationRules: [],
        showHelp: false,
        repeatable: false,
    },
    {
        id: VALUES.TEXT,
        component: 'Text',
        title: 'Nota',
        validationRules: [],
        showHelp: false,
        repeatable: false,
    },
    {
        id: VALUES.INPUT,
        component: 'TextInput',
        title: 'Tekst (één lijn)',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
            { title: 'E-mail', id: 'email' },
            { title: 'URL', id: 'url' },
            { title: 'Minimum karakters', id: 'min', extra: true },
            { title: 'Maximum karakters', id: 'max', extra: true },
            { title: 'Reguliere expressie', id: 'regex', extra: true },
        ],
        showHelp: true,
        repeatable: true,
    },
    {
        id: VALUES.TEXTAREA,
        component: 'TextArea',
        title: 'Tekst (meerdere lijnen)',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
            { title: 'Minimum karakters', id: 'min', extra: true },
            { title: 'Maximum karakters', id: 'max', extra: true },
            { title: 'Reguliere expressie', id: 'regex', extra: true },
        ],
        showHelp: true,
        repeatable: false,
    },
    {
        id: VALUES.NUMBER,
        component: 'Number',
        title: 'Getal',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: true,
    },
    {
        id: VALUES.DATE,
        component: 'Date',
        title: 'Datum',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: true,
    },
    {
        id: VALUES.DATETIME,
        component: 'DateTime',
        title: 'Datum & tijd',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: true,
    },
    {
        id: VALUES.BOOLEAN,
        component: 'Boolean',
        title: 'Één checkbox',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: false,
    },
    {
        id: VALUES.CHECKBOX,
        component: 'Options',
        title: 'Meerkeuze (meer opties mogelijk)',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
            { title: 'Minimum aantal aan te duiden opties', id: 'min', extra: true },
        ],
        showHelp: true,
        repeatable: false,
    },
    {
        id: VALUES.RADIO,
        component: 'Options',
        title: 'Meerkeuze (één optie mogelijk)',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: false,
    },
    {
        id: VALUES.SELECT,
        component: 'Options',
        title: 'Meerkeuze (één optie mogelijk - dropdown)',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: false,
    },
    {
        id: VALUES.FILE,
        component: 'File',
        title: 'Bestanden',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
            { title: 'Bestandtype',
                id: 'mimes',
                extra: true,
                help: 'Een lijst van extenties, gescheiden door een komma' },
        ],
        showHelp: true,
        repeatable: false,
    },
    {
        id: VALUES.LATLNG,
        title: 'Locatie',
        component: 'Location',
        validationRules: [
            { title: 'Verplicht', id: 'required' },
        ],
        showHelp: true,
        repeatable: true,
    },
    {
        id: VALUES.DOWNLOAD,
        title: 'Download',
        component: 'Download',
        validationRules: [],
        showHelp: true,
        repeatable: false,
    },
];

export default {
    enum: VALUES,

    values: FIELD_TYPES,

    getByType: (type) => FIELD_TYPES.find(({ id }) => id === type),
};
