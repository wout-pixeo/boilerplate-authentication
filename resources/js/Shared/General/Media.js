import get from 'lodash/get';

export default class Media {
    constructor(values) {
        this.reference = get(values, 'reference');
        this.created_at = get(values, 'created_at');
        this.extension = get(values, 'extension');
        this.file_name = get(values, 'file_name');
        this.is_image = get(values, 'is_image');
        this.name = get(values, 'name');
        this.success = get(values, 'success');
    }

    toJSON() {
        return {
            reference: this.reference,
            created_at: this.created_at,
            extension: this.extension,
            file_name: this.file_name,
            is_image: this.is_image,
            name: this.name,
            success: this.success,
            url: `/temp_uploads/${this.reference}/${encodeURI(this.file_name)}`,
        };
    }
}
