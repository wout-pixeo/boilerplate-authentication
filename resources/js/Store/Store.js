import Vue from 'vue';

const createStore = ({ state }) => new Vue({
    data() {
        return { state };
    },
});

export default createStore;
