export default {
    MODULE_ITEM: {
        CREATED: {
            id: 'CREATED',
            class: 'badge-gray',
            label: 'Vul in',
            description: 'Het dossier staat klaar om in te vullen. Vul het dossier verder aan.',
        },
        SUBMITTED: {
            id: 'SUBMITTED',
            class: 'badge-blue',
            label: 'In behandeling',
            description: 'Het dossier is ingediend.',
        },
        APPROVED: {
            id: 'APPROVED',
            class: 'badge-green',
            label: 'Goedgekeurd',
            description: 'Het dossier is goedgekeurd.',
        },
        DISAPPROVED: {
            id: 'DISAPPROVED',
            class: 'badge-red',
            label: 'Afgekeurd',
            description: 'Het dossier is afgekeurd.',
        },
    },
    FORM: {
        CREATED: {
            id: 'CREATED',
            class: 'badge-gray',
            label: 'Vul in',
            description: 'Het formulier staat klaar om in te vullen. Vul het formulier verder aan.',
        },
        DRAFT: {
            id: 'DRAFT',
            class: 'badge-orange',
            label: 'Bewaard, niet ingediend',
            description: 'Er zijn al enkele gegevens ingevuld en bewaard. Vul de ontbrekende gegevens verder aan.',
        },
        SUBMITTED: {
            id: 'SUBMITTED',
            class: 'badge-blue',
            label: 'In behandeling',
            description: 'Het formulier is ingediend.',
        },
        APPROVED: {
            id: 'APPROVED',
            class: 'badge-green',
            label: 'Goedgekeurd',
            description: 'Het formulier is goedgekeurd.',
        },
        CONDITIONALAPPROVED: {
            id: 'CONDITIONALAPPROVED',
            class: 'badge-green',
            label: 'Goedgekeurd met voorwaarden',
            description: 'Het formulier is goedgekeurd met voorwaarde(n).',
        },
        DISAPPROVED: {
            id: 'DISAPPROVED',
            class: 'badge-red',
            label: 'Afgekeurd',
            description: 'Het formulier is afgekeurd.',
        },
    },
    ADVICE: {
        POSITIVE: {
            id: 'POSITIVE',
            class: 'badge-green',
            label: 'Positief advies',
            description: 'Positief advies.',
        },
        NEGATIVE: {
            id: 'NEGATIVE',
            class: 'badge-red',
            label: 'Negatief advies',
            description: 'Negatief advies.',
        },
    },
};
