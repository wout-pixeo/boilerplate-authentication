import createStore from './Store';
import Statusses from './Statusses';

export default createStore({
    state: {
        statusses: Statusses,
    },
});
