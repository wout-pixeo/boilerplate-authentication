@component('mail::message')
# Wachtwoord opnieuw instellen

Hallo {{ $user->name }},

Je hebt een nieuw wachtwoord aangevraagd op [{{ Request::getHost() }}]({{ route('password.reset', [$token]) }}?email={{ urlencode($user->email) }}).

@component('mail::button', ['url' => route('password.reset', [$token]).'?email='.urlencode($user->email)])
Wijzig wachtwoord
@endcomponent

@component('mail::panel')
Wil je je wachtwoord niet opnieuw instellen? Negeer dan dit bericht, je oude wachtwoord werkt nog steeds!
@endcomponent

@slot('subcopy')
Wijzig je wachtwoord voor {{ Carbon\Carbon::now()->addDays(3)->format('d/m/Y') }}.
@endslot
@endcomponent
