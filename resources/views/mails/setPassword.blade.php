@component('mail::message')
# Wachtwoord instellen

Hallo {{ $user->name }},

Je hebt toegang gekregen tot [{{ config('app.url') }}]({{ action('Auth\ResetPasswordController@showResetForm', [$token]) }}?email={{ urlencode($user->email) }}).

@component('mail::button', ['url' => action('Auth\ResetPasswordController@showResetForm', [$token]).'?email='.urlencode($user->email)])
Wachtwoord instellen
@endcomponent

@slot('subcopy')
Stel je wachtwoord in voor {{ Carbon\Carbon::now()->addDays(3)->format('d/m/Y') }}.
@endslot
@endcomponent
