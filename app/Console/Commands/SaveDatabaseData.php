<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SaveDatabaseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flowlab:dump-db-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dumps database data into a seed file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $database = env('DB_DATABASE');

        $ts = time();

        $path = database_path('sql').DIRECTORY_SEPARATOR;
        $file = date('Y-m-d-His', $ts) . '-dump-data-' . $database . '.sql';
        $command = sprintf(
            'mysqldump --no-create-info  -h %s -u %s %s %s --ignore-table=%s.migrations > %s',
            $host,
            $username,
            $password ? '-p\''.$password.'\'' : '',
            $database,
            $database,
            $path . $file
        );

        exec($command);
    }
}
