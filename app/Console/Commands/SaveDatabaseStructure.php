<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SaveDatabaseStructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flowlab:dump-db-structure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dumps database structure into a seed file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $database = env('DB_DATABASE');

        $ts = time();

        $path = database_path('sql').DIRECTORY_SEPARATOR;
        $file = date('Y-m-d-His', $ts) . '-dump-structure-' . $database . '.sql';
        $command = sprintf(
            'mysqldump --no-data -h %s -u %s %s %s > %s',
            $host,
            $username,
            $password ? '-p\''.$password.'\'' : '',
            $database,
            $path . $file
        );

        exec($command);
    }
}
