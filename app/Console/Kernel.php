<?php

namespace App\Console;

use App\Console\Commands\SaveDatabaseData;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\SaveDatabaseStructure;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Domain\Workflows\Commands\ExecuteWorkflowItemsWithPassedDueDate;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ExecuteWorkflowItemsWithPassedDueDate::class,
        SaveDatabaseData::class,
        SaveDatabaseStructure::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('flowlab:execute-workflow-item-actions')
            ->everyFiveMinutes();

        if ($this->app->environment('testing')) {
            $schedule->command('migrate:fresh', ['--seeder=FileSeeder', '--force'])
                ->dailyAt('00:00');
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
