<?php

namespace App\Providers;

use Inertia\Inertia;
use Carbon\CarbonImmutable;
use App\Domain\Users\Enums\Role;
use Illuminate\Support\Collection;
use App\Domain\Cities\Models\Domain;
use Illuminate\Pagination\UrlWindow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\LengthAwarePaginator;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerInertia();
        $this->registerLengthAwarePaginator();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setRootUrl();

        Date::use(CarbonImmutable::class);
    }

    public function registerInertia()
    {
        Inertia::version(function () {
            return md5_file(public_path('mix-manifest.json'));
        });

        // @TODO Refactor this so it's more compact and elegant

        Inertia::share([
            'appName' => config('app.name'),
            'env' => config('app.env'),
            'auth' => function () {
                return [
                    'user' => Auth::user() ? [
                        'id' => Auth::user()->id,
                        'first_name' => Auth::user()->first_name,
                        'last_name' => Auth::user()->last_name,
                        'email' => Auth::user()->email,
                        'avatar' => Auth::user()->avatar,
                        'is_organiser' => Auth::user()->hasRole(Role::user()->getValue()),
                        'impersonating' => Auth::user()->isImpersonating(),
                    ] : null,
                ];
            },
            'flash' => function () {
                return [
                    'success' => Session::get('success'),
                ];
            },
            'errors' => function () {
                return Session::get('errors')
                    ? Session::get('errors')->getBag('default')->getMessages()
                    : (object) [];
            },
            'breadcrumbs' => function () {
                return Breadcrumbs::generate();
            },
            'route' => function () {
                return [
                    'route' => request()->route()->getName(),
                    'params' => request()->route()->parameters(),
                ];
            },
            'menu' => function () {
                $menu = include_once base_path('routes/menu.php');

                return $menu;
            },
        ]);
    }

    protected function registerLengthAwarePaginator()
    {
        $this->app->bind(LengthAwarePaginator::class, function ($app, $values) {
            return new class(...array_values($values)) extends LengthAwarePaginator {
                public function only(...$attributes)
                {
                    return $this->transform(function ($item) use ($attributes) {
                        return $item->only($attributes);
                    });
                }

                public function transform($callback)
                {
                    $this->items->transform($callback);

                    return $this;
                }

                public function toArray()
                {
                    return [
                        'data' => $this->items->toArray(),
                        'links' => $this->links(),
                    ];
                }

                public function links($view = null, $data = [])
                {
                    $this->appends(Request::all());

                    $window = UrlWindow::make($this);

                    $elements = array_filter([
                        $window['first'],
                        is_array($window['slider']) ? '...' : null,
                        $window['slider'],
                        is_array($window['last']) ? '...' : null,
                        $window['last'],
                    ]);

                    return Collection::make($elements)->flatMap(function ($item) {
                        if (is_array($item)) {
                            return Collection::make($item)->map(function ($url, $page) {
                                return [
                                    'url' => $url,
                                    'label' => $page,
                                    'active' => $this->currentPage() === $page,
                                ];
                            });
                        } else {
                            return [
                                [
                                    'url' => null,
                                    'label' => '...',
                                    'active' => false,
                                ],
                            ];
                        }
                    })->prepend([
                        'url' => $this->previousPageUrl(),
                        'label' => 'Vorige',
                        'active' => false,
                    ])->push([
                        'url' => $this->nextPageUrl(),
                        'label' => 'Volgende',
                        'active' => false,
                    ]);
                }
            };
        });
    }

    protected function setRootUrl()
    {
        if ($this->app->runningInConsole()) {
            return;
        }

        $domain = Domain::where('url', request()->getSchemeAndHttpHost())->first();

        if ($domain) {
            $this->app->instance('App\CurrentCity', $domain->city);

            // Replace media library urls with the current url
            $disk = $this->app['config']->get('medialibrary.disk_name');
            $url = $this->app['config']->get("filesystems.disks.{$disk}.url");
            $url = str_replace($this->app['config']->get('app.url'), $domain->url, $url);

            $this->app['config']->set("filesystems.disks.{$disk}.url", $url);
            $this->app['config']->set('app.url', $domain->url);
        }
    }
}
