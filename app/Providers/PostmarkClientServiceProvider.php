<?php

namespace App\Providers;

use Carbon\CarbonImmutable;
use Postmark\PostmarkAdminClient;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;

class PostmarkClientServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Date::use(CarbonImmutable::class);

        $this->app->bind(PostmarkAdminClient::class, function ($app) {
            return new PostmarkAdminClient($app['config']->get('mail.postmark_api_key'));
        });
    }
}
