<?php

namespace App\Domain\Workflows\Commands;

use Illuminate\Console\Command;

class ExecuteWorkflowItemsWithPassedDueDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flowlab:execute-workflow-item-actions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute workflow items actions with passed due dates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new \App\Domain\Workflows\Actions\ExecuteWorkflowItemsWithPassedDueDate())->execute();
    }
}
