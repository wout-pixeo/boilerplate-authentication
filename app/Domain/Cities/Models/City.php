<?php

namespace App\Domain\Cities\Models;

use App\Services\Eloquent\Model;
use App\Services\Eloquent\Traits\Sortable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Services\Eloquent\Traits\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class City extends Model implements HasMedia
{
    use HasMediaTrait;
    use Sortable;
    use SoftDeletes;
    use Filterable;
    use LogsActivity;

    public $files = ['logo_heading', 'logo', 'bg_intro'];

    protected $fillable = [
        'name', 'street', 'number', 'box', 'postcode', 'city',
        'country_id', 'email', 'phone', 'website', 'intro',
        'footer', 'privacy_link', 'disclaimer_link', 'color',
        'postmark_sender_signature_id', 'postmark_sender_signature_email',
        'postmark_sender_signature_name', 'postmark_sender_signature_reply_to',
    ];

    protected static $logFillable = true;

    protected static $logOnlyDirty = true;

    protected static $filterable = ['name', 'street', 'number', 'postcode', 'city', 'email', 'phone'];

    protected $appends = ['logo_heading', 'logo', 'bg_intro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function domains()
    {
        return $this->hasMany(Domain::class);
    }

    public function getLogoHeadingAttribute()
    {
        return $this->getMedia('logo_heading')->pluck('id')->toArray();
    }

    public function getLogoAttribute()
    {
        return $this->getMedia('logo')->pluck('id')->toArray();
    }

    public function getBgIntroAttribute()
    {
        return $this->getMedia('bg_intro')->pluck('id')->toArray();
    }
}
