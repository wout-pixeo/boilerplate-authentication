<?php

namespace App\Domain\Cities\Models;

use App\Services\Eloquent\Model;
use App\Services\Eloquent\Traits\Sortable;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Services\Eloquent\Traits\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Domain extends Model
{
    use Sortable;
    use Filterable;
    use SoftDeletes;
    use LogsActivity;

    protected $fillable = ['city_id', 'url', 'default'];

    protected static $defaultOrderBy = 'url';

    protected static $filterable = ['url'];

    protected static $logFillable = true;

    protected static $logOnlyDirty = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
