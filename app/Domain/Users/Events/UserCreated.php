<?php

namespace App\Domain\Users\Events;

use App\Domain\Users\Models\User;

class UserCreated
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
