<?php

namespace App\Domain\Users\Models;

use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;
use App\Domain\Users\Mail\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Nckg\Impersonate\Traits\CanImpersonate;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Services\Eloquent\Traits\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use Filterable;
    use LogsActivity;
    use CanImpersonate;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'email_verified_at',
    ];

    protected static $logAttributes = [
        'first_name', 'last_name', 'email',
    ];

    protected static $logOnlyDirty = true;

    protected static $filterable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    protected $required = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasNeverLoggedIn(): bool
    {
        return empty($this->password);
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->email)->send(new ResetPassword($this, $token));
    }

    /**
     * Checks if a user has all it's fields filled out.
     *
     * @return bool
     */
    public function isComplete()
    {
        foreach ($this->required as $key) {
            if (! $this->getAttribute($key)) {
                return false;
            }
        }

        return true;
    }

    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getAvatarHash()
    {
        return md5($this->email);
    }

    public function getAvatarAttribute()
    {
        return sprintf(
            "https://www.gravatar.com/avatar/%s?d=%s",
            $this->getAvatarHash(),
            config('app.env') === 'production'
                ? urlencode(route('avatar', ['name' => $this->name]))
                : urlencode("https://ui-avatars.com/api/{$this->name}/128")
        );
    }
}
