<?php

namespace App\Domain\Users\ViewModels;

use App\Domain\Users\Enums\Role as RoleEnum;

class Role
{
    public static function getLabel(RoleEnum $status)
    {
        $classes = [
            'Root',
            'Beheerder',
            'Gebruiker',
        ];

        return $classes[$status->getIndex()];
    }
}
