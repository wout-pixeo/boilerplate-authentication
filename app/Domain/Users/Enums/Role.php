<?php

namespace App\Domain\Users\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self root()
 * @method static self admin()
 * @method static self user()
 */
class Role extends Enum
{
}
