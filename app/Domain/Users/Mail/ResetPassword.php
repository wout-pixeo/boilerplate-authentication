<?php

namespace App\Domain\Users\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Domain\Users\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;

    public $token;

    public function __construct(User $user, string $token)
    {
        $this->user = $user;

        $this->token = $token;
    }

    public function build()
    {
        return $this
            ->subject('🔐 Toegang tot '.config('app.url'))
            ->markdown($this->user->hasNeverLoggedIn() ? 'mails.setPassword' : 'mails.resetPassword');
    }
}
