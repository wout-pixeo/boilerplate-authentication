<?php

namespace App\Domain\Media\Models;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class UploadedTempFile implements Jsonable
{
    public $reference = null;
    public $fileName = null;
    public $id = null;

    public function __construct(array $keys = null)
    {
        $this->reference = data_get($keys, 'reference');
        $this->fileName = data_get($keys, 'file_name');
        $this->id = data_get($keys, 'id');
    }

    public function path(): string
    {
        return DIRECTORY_SEPARATOR . $this->reference . DIRECTORY_SEPARATOR . $this->fileName;
    }

    public function exists()
    {
        return $this->disk()->exists($this->path());
    }

    public function file()
    {
        if ($this->exists()) {
            $storagePath  = $this->disk()->getDriver()->getAdapter()->getPathPrefix();

            return new UploadedFile($storagePath . $this->path(), $this->fileName);
        }

        throw new FileNotFoundException($this->path());
    }

    public function hasFile()
    {
        return ! ! $this->fileName;
    }

    /**
     * Check if asset's file extension is one of a given list
     *
     * @param array $filetypes
     * @return bool
     */
    public function extensionIsOneOf($extension, $filetypes = [])
    {
        return (in_array(strtolower($extension), $filetypes));
    }

    public function toArray()
    {
        return [
            'success' => true,
            'file_name' => $this->fileName,
            'name' => $this->file()->getFilename(),
            'extension' => $this->file()->extension(),
            'is_image' => $this->extensionIsOneOf($this->file()->extension(), ['jpg', 'jpeg', 'png', 'gif', 'svg']),
            'reference' => $this->reference,
            'created_at' => $this->file()->getMTime(),
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    protected function disk()
    {
        return Storage::disk('tmp');
    }
}
