<?php

namespace App\Domain\Media\Models;

use Spatie\MediaLibrary\Models\Media as Model;

class Media extends Model
{
    /**
     * Is this asset an image?
     *
     * @return bool
     */
    public function isImage()
    {
        return  $this->extensionIsOneOf(['jpg', 'jpeg', 'png', 'gif', 'svg']);
    }

    /**
     * Is this asset a video file?
     *
     * @return bool
     */
    public function isVideo()
    {
        return $this->extensionIsOneOf(['h264', 'mp4', 'm4v', 'ogv', 'webm']);
    }

    /**
     * Check if asset's file extension is one of a given list
     *
     * @param array $filetypes
     * @return bool
     */
    public function extensionIsOneOf($filetypes = [])
    {
        return (in_array(strtolower($this->extension), $filetypes));
    }
}
