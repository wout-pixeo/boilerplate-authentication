<?php

namespace App\Domain\Media\Actions;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use App\Domain\Media\Models\UploadedTempFile;

class UploadFile
{
    /**
     * @param UploadedFile $file
     * @return UploadedTempFile
     */
    public function execute(UploadedFile $file)
    {
        $fileName = $file->getClientOriginalName();

        $reference = Str::random();

        $file
            ->storeAs(
                $reference,
                $fileName,
                ['disk' => 'tmp']
            );

        return new UploadedTempFile([
            'reference' => $reference,
            'file_name' => $fileName,
        ]);
    }
}
