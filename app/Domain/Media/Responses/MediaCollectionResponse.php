<?php

namespace App\Domain\Media\Responses;

use App\Domain\Media\Models\Media;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Responsable;

class MediaCollectionResponse implements Responsable
{
    /**
     * @var Collection
     */
    public $media;

    /**
     * MediaCollectionResponse constructor.
     * @param Collection $media
     */
    public function __construct(Collection $media)
    {
        $this->media = $media;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        $result = $this->transformMedia();

        return response()->json($result->count() > 1 ? $result : $result->first());
    }

    /**
     * @return Collection
     */
    protected function transformMedia()
    {
        return $this->media->map(function (Media $model) {
            return [
                'id' => $model->id,
                'collection' => $model->collection_name,
                'extension' => $model->extension,
                'file_name' => $model->file_name,
                'name' => $model->name,
                'url' => $model->getUrl(),
                'original_url' => $model->getUrl(),
                'size' => $model->size,
                'mime' => $model->mime_type,
                'is_image' => $model->isImage(),
                'is_video' => $model->isVideo(),
                'created_at' => $model->created_at,
            ];
        });
    }
}
