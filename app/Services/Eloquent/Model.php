<?php

namespace App\Services\Eloquent;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    protected static $defaultOrderBy = 'name';
}
