<?php

namespace App\Services\Eloquent\Traits;

use Illuminate\Support\Arr;

trait Sortable
{
    public function scopeSort($query, array $filters)
    {
        $orderBy = Arr::get($filters, 'order_by', static::$defaultOrderBy);
        $orderByDir = Arr::get($filters, 'order_by_dir', 'asc');

        $query->orderBy($orderBy, $orderByDir);
    }
}
