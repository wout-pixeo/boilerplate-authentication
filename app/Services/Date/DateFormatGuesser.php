<?php

namespace App\Services\Date;

use Illuminate\Support\Arr;

/**
 * Class DateFormatGuesser
 * @package App\Services
 *
 */
class DateFormatGuesser
{
    protected static $matchers = [
        'H:i' => '^(\d{1,2}):(\d{1,2})$',
        'd/m/Y' => '^(\d{1,2})/(\d{1,2})/(\d{4})$',
        'd/m/Y H:i' => '^(\d{1,2})/(\d{1,2})/(\d{4}) (\d{1,2}):(\d{1,2})$',
        'd/m/Y H:i:s' => '^(\d{1,2})/(\d{1,2})/(\d{4}) (\d{1,2}):(\d{1,2}):(\d{1,2})$',
        'Y-m-d H:i' => '^(\d{1,2})/(\d{1,2})/(\d{4}) (\d{1,2}):(\d{1,2})$',
    ];

    /**
     * @param $format
     * @return mixed
     * @throws \Exception
     */
    public static function guess($format)
    {
        $result = array_filter(static::$matchers, function ($pattern) use ($format) {
            return preg_match('#'.$pattern.'#', $format);
        });

        if (empty($result)) {
            throw new \Exception("Format not found for {$format}");
        }

        return Arr::first(array_keys($result));
    }
}
