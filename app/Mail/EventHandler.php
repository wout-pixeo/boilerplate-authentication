<?php

namespace App\Mail;

use App\Domain\Users\Events\UserCreated;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Events\Dispatcher;

class EventHandler
{
    public function subscribe(Dispatcher $events)
    {
        // Listen to the user created function
        $events->listen(UserCreated::class, function (UserCreated $event) {
            Password::broker()->sendResetLink(['email' => $event->user->email]);
        });
    }
}
