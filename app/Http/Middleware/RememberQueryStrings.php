<?php

namespace App\Http\Middleware;

use Illuminate\Support\Str;
use Reinink\RememberQueryStrings as Middleware;

class RememberQueryStrings extends Middleware
{
    protected function remembered($next, $request)
    {
        $remembered = array_filter($request->session()->get($this->getKey($request)) ?? []);

        if ($remembered) {
            $request->session()->reflash();

            return redirect(url($request->path()).'?'.http_build_query($remembered));
        }

        return $next($request);
    }

    protected function remember($next, $request)
    {
        $request->session()->put($this->getKey($request), array_filter($request->all()));

        return $next($request);
    }

    protected function forget($next, $request)
    {
        $request->session()->remove($this->getKey($request));

        return redirect(url($request->path()));
    }

    protected function getKey($request)
    {
        return 'remember_query_strings.'.Str::slug($request->path());
    }
}
