<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\UpdateDates;
use App\Http\Controllers\Traits\UpdateFillable;
use App\Services\ValidationRules\NationalRegistryNumber;

class CompleteAccountController extends Controller
{
    use UpdateFillable;
    use UpdateDates;

    public function show(Request $request)
    {
        if ($request->user()->isComplete()) {
            return response()->redirectToRoute('home');
        }

        return inertia('Auth/CompleteAccount')->with('model', $request->user());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => ['required', 'min:2'],
            'last_name' => ['required', 'min:2'],
            'street' => ['required', 'min:2'],
            'number' => ['required', 'numeric'],
            'postcode' => ['required'],
            'city' => ['required'],
            'country_id' => ['required'],
            'birth_date' => ['required', 'date_format:Y-m-d H:i'],
            'national_registry_number' => [new NationalRegistryNumber()],
        ]);

        $model = $request->user();

        $this->updateFillable($model, $request);

        $this->updateDates($model, $request);

        $model->save();

        return redirect()
            ->to(route('dashboard.index'))
            ->with('success', 'Gebruiker succesvol bewaard');
    }
}
