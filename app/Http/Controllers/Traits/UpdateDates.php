<?php

namespace App\Http\Controllers\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Services\Date\DateFormatGuesser;

trait UpdateDates
{
    /**
     * @param Model $model
     * @param Request $request
     */
    protected function updateDates(Model $model, Request $request)
    {
        foreach ($model->getDates() as $attr) {
            if (! $request->exists($attr)) {
                continue;
            }

            if (is_a($request->input($attr), Carbon::class)) {
                $model->$attr = $request->input($attr);
                continue;
            }

            if (empty($request->input($attr))) {
                $model->$attr = null;
                continue;
            }

            if (is_array($request->input($attr))) {
                $value = implode(' ', $request->input($attr));
                $model->$attr = Carbon::createFromFormat(DateFormatGuesser::guess($value), $value);
                continue;
            }

            try {
                $model->$attr = Carbon::createFromFormat(
                    DateFormatGuesser::guess($request->input($attr)),
                    $request->input($attr)
                );
            } catch (\Exception $exception) { // ignore exception
                $model->$attr = Carbon::parse($request->input($attr));
            }
        }
    }
}
