<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

trait UpdateFillable
{
    /**
     * @param Model $model
     * @param Request $request
     */
    protected function updateFillable(Model $model, Request $request)
    {
        $keys = array_diff($model->getFillable(), $model->getDates());

        foreach ($keys as $key) {
            if ($request->exists($key)) {
                $model->$key = $request->input($key);
            }
        }
    }
}
