<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Database\Eloquent\Model;

trait DispatchesEvent
{
    /**
     * @param Model $model
     * @param $type
     * @throws \ReflectionException
     */
    public function dispatchEvent($type, Model $model)
    {
    }

    /**
     * @param Model $model
     * @param $type
     * @return string
     * @throws \ReflectionException
     */
    protected function determineEventName(Model $model, $type)
    {
    }
}
