<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Media\Models\UploadedTempFile;

trait UploadFiles
{
    protected function uploadFiles(Model $model, array $value, $collection = 'default', $disk = 's3')
    {
        $items = [];
        foreach ($value as $item) {
            $items[] = $this->uploadFile($model, $item, $collection, $disk);
        }

        return $items;
    }

    protected function uploadFile(Model $model, $value, $collection = 'default', $disk = 's3')
    {
        if (is_numeric($value)) {
            return $value;
        }

        $file = new UploadedTempFile($value);

        if ($file->id) {
            return $file->id;
        }

        if (! $file->exists() || ! $file->hasFile()) {
            return null;
        }

        $media = $model
            ->addMedia($file->file())
            ->toMediaCollection($collection, $disk);

        return $media->id;
    }
}
