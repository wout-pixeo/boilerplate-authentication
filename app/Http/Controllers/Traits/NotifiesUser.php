<?php

namespace App\Http\Controllers\Traits;

trait NotifiesUser
{
    /**
     * @param $model
     * @param $type
     */
    protected function triggerEventsFor($model, $type)
    {
        $this->logActivity($type, $model);
    }

    /**
     * @param string $type
     * @return string
     */
    protected function getEventDescriptionFor($type)
    {
        return 'models.'.strtolower($this->moduleName).'.'.$type;
    }

    /**
     * @param $model
     * @param $type
     */
    protected function logActivity($type, $model = null)
    {
//        if (is_null($model)) {
////            activity()->log($this->getEventDescriptionFor($type));
//
//            return;
//        }

//        activity()->on($model)->log($this->getEventDescriptionFor($type));
    }
}
