<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController
{
    public function index(Request $request)
    {
        return inertia('Dashboard/Index');
    }
}
