<?php

namespace App\Http\Controllers;

use Laravolt\Avatar\Facade as Avatar;

class AvatarController
{
    public function index($name)
    {
        $image = Avatar::create($name)->toBase64();
        $image = str_replace('data:image/png;base64,', '', $image);

        $response = response()->make(base64_decode($image), 200);
        $response->header('Content-Type', 'image/png');

        return $response;
    }
}
