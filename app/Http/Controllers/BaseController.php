<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Traits\UpdateDates;
use App\Http\Controllers\Traits\NotifiesUser;
use App\Http\Controllers\Traits\UpdateFillable;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Traits\DispatchesEvent;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class BaseController extends Controller
{
    use AuthorizesRequests;
    use ValidatesRequests;
    use UpdateDates;
    use UpdateFillable;
    use DispatchesEvent;
    use NotifiesUser;

    /**
     * @var string
     */
    protected $modelClass;

    protected $modelTitle = 'title';

    /**
     * @var string
     */
    protected $moduleName;

    /**
     * @var string
     */
    protected $viewPrefix = '';

    /**
     * Controller constructor
     */
    public function __construct()
    {
        $this->moduleName = $this->determineModuleName();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $models = $this->transformAll($this->all());

        return inertia("{$this->viewPrefix}{$this->moduleName}/Index", [
            'models' => $models,
            'filters' => $request->all('order_by', 'order_by_dir', 'search', 'trashed'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        return inertia("{$this->viewPrefix}{$this->moduleName}/Create");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|Job
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules($request));

        $model = $this->make();

        $this->createFromRequest($model, $request);

        $this->dispatchEvent('created', $model);

        $this->triggerEventsFor($model, 'created');

        return redirect()
            ->to($this->redirectTo())
            ->with('success', $this->getSuccessMessage());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Request $request)
    {
        $model = $this->find((int) $request->id);

        return inertia("{$this->viewPrefix}{$this->moduleName}/Edit", [
            'model' => $this->transformModel($model),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Request $request)
    {
        $model = $this->find((int) $request->route('id'));

        $model = $this->transformModel($model);

        return inertia("{$this->viewPrefix}{$this->moduleName}/Show", compact('model'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request)
    {
        $model = $this->find((int) $request->route('id'));

        $this->validate($request, $this->validationRules($request, $model));

        $this->updateFromRequest($model, $request);

        $this->dispatchEvent('updated', $model);

        $this->triggerEventsFor($model, 'updated');

        return redirect()
            ->to($this->redirectTo())
            ->with('success', $this->getSuccessMessage());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Request $request)
    {
        $model = $this->find((int) $request->route('id'));

        $model->delete();

        $this->dispatchEvent('deleted', $model);

        $this->triggerEventsFor($model, 'deleted');

        return redirect()
            ->to($this->redirectTo())
            ->with('success', $this->getDeleteSuccessMessage());
    }

    /**
     * Returns the redirect route when logged in
     *
     * @return string
     */
    public function redirectTo()
    {
        return $this->action('index');
    }

    /**
     * Determines name of the module
     *
     * @return string
     */
    protected function determineModuleName(): string
    {
        return ucfirst(Str::replaceLast('Controller', '', class_basename($this)));
    }

    /**
     * @param Model $model
     * @param Request $request
     */
    protected function updateModel(Model $model, Request $request)
    {
        $this->updateFillable($model, $request);

        $this->updateDates($model, $request);

        $model->save();
    }

    /**
     * Return an action
     *
     * @param string $action
     * @param array $parameters
     * @return string
     */
    protected function action(string $action, $parameters = []): string
    {
        return action('\\'.static::class.'@'.$action, $parameters);
    }

    /**
     * @param int $id
     * @return mixed
     */
    protected function find(int $id): Model
    {
        return call_user_func([$this->modelClass, 'where'], 'id', $id)
            ->firstOrFail();
    }

    /**
     * @return mixed
     */
    protected function all()
    {
        return call_user_func([$this->modelClass, 'orderBy'], 'id', 'desc')
            ->paginate();
    }

    /**
     * @param Request $request
     * @param Model|null $model
     * @return array
     */
    protected function validationRules(Request $request, $model = null): array
    {
        return [];
    }

    /**
     * @return mixed
     */
    protected function make()
    {
        return new $this->modelClass;
    }

    protected function transformAll(LengthAwarePaginator $paginator)
    {
        return $paginator;
    }

    protected function transformModel(Model $model)
    {
        return $model;
    }

    /**
     * @return array|string|\Underscore\Underscore|null
     */
    protected function getSuccessMessage()
    {
        return __('Het item is succesvol bewaard');
    }

    /**
     * @return array|string|\Underscore\Underscore|null
     */
    protected function getDeleteSuccessMessage()
    {
        return __('Het item is succesvol verwijderd');
    }

    abstract protected function createFromRequest($model, Request $request);

    abstract protected function updateFromRequest(Model $model, Request $request);
}
