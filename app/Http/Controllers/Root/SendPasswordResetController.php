<?php

namespace App\Http\Controllers\Root;

use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use App\Domain\Users\Events\UserCreated;

class SendPasswordResetController extends Controller
{
    public function store($id)
    {
        $model = User::find($id);

        event(new UserCreated($model));

        return back()->with('success', 'De e-mail werd succesvol verzonden');
    }
}
