<?php

namespace App\Http\Controllers\Root;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Domain\Cities\Models\City;
use App\Domain\Cities\Models\Domain;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\BaseController;
use Illuminate\Pagination\LengthAwarePaginator;

class DomainsController extends BaseController
{
    protected $modelClass = Domain::class;

    protected $viewPrefix = 'Root/';

    public function create(Request $request)
    {
        return parent::create($request)->with('cities', City::orderBy('name')->get());
    }

    public function edit(Request $request)
    {
        return parent::edit($request)->with('cities', City::orderBy('name')->get());
    }

    protected function createFromRequest($model, Request $request)
    {
        $this->setDefault($request);

        $this->updateModel($model, $request);
    }

    protected function updateFromRequest(Model $model, Request $request)
    {
        $this->setDefault($request);

        $this->updateModel($model, $request);
    }

    protected function validationRules(Request $request, $model = null): array
    {
        $rules = [
            'url' => ['required'],
            'city_id' => 'required',
        ];

        $rules['url'][] = $model
            ? Rule::unique('domains', 'url')->ignore($model->id)
            : 'unique:domains,url';

        return $rules;
    }

    /**
     * @param Request $request
     */
    protected function setDefault(Request $request): void
    {
        if ($request->input('default')) {
            Domain::where('city_id', $request->input('city_id'))->update(['default' => false]);
        }
    }

    /**
     * @return mixed
     */
    protected function all()
    {
        return call_user_func([$this->modelClass, 'sort'], request()->only('order_by', 'order_by_dir'))
            ->filter(request()->only('search', 'trashed', 'status'))
            ->with('city')
            ->paginate();
    }

    protected function transformAll(LengthAwarePaginator $paginator)
    {
        return parent::transformAll($paginator)->transform(function (Domain $model) {
            return [
                'id' => $model->id,
                'url' => $model->url,
                'default' => $model->default,
                'city' => $model->city->name,
            ];
        });

        return parent::transformAll($paginator);
    }
}
