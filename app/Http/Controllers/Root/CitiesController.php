<?php

namespace App\Http\Controllers\Root;

use Illuminate\Http\Request;
use Postmark\PostmarkAdminClient;
use App\Domain\Cities\Models\City;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Traits\UploadFiles;
use Illuminate\Pagination\LengthAwarePaginator;

class CitiesController extends BaseController
{
    use UploadFiles;

    protected $modelClass = City::class;

    protected $viewPrefix = 'Root/';

    private $client;

    public function __construct(PostmarkAdminClient $client)
    {
        $this->client = $client;

        parent::__construct();
    }

    protected function createFromRequest($model, Request $request)
    {
        /** @var $model City */
        $this->updateModel($model, $request);

        $this->upload($model, $request);
    }

    protected function updateFromRequest(Model $model, Request $request)
    {
        $response = $this->client->getSenderSignature($request->input('postmark_sender_signature_id'));

        $model->postmark_sender_signature_id = $response->Id;
        $model->postmark_sender_signature_email = $response->EmailAddress;
        $model->postmark_sender_signature_name = $response->Name;
        $model->postmark_sender_signature_reply_to = $response->ReplyToEmailAddress;

        $this->updateModel($model, $request);

        $this->upload($model, $request);
    }

    protected function validationRules(Request $request, $model = null): array
    {
        return [
            'name' => 'required',
            'street' => 'required',
            'number' => 'required',
            'postcode' => 'required',
            'city' => 'required',
            'country_id' => 'required',
            'email' => ['email', 'required'],
            'phone' => 'required',
            'website' => ['url', 'required'],
            'privacy_link' => ['sometimes', 'url'],
            'disclaimer_link' => ['sometimes', 'url'],
            'postmark_sender_signature_id' => 'required',
        ];
    }

    protected function transformAll(LengthAwarePaginator $paginator)
    {
        return $paginator->transform(function (City $model) {
            return [
                'id' => $model->id,
                'name' => $model->name,
            ];
        });
    }

    protected function all()
    {
        return call_user_func([$this->modelClass, 'sort'], request()->only('order_by', 'order_by_dir'))
            ->filter(request()->only('search', 'trashed', 'status'))
            ->paginate();
    }

    /**
     * @param Model $model
     * @param Request $request
     */
    protected function upload(Model $model, Request $request): void
    {
        collect($model->files)->each(function ($key) use ($model, $request) {
            $result = $this->uploadFile($model, array_first($request->input($key)), $key, 'public');

            if (! $result) {
                $model->clearMediaCollection($key);
            }
        });
    }
}
