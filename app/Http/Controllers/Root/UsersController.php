<?php

namespace App\Http\Controllers\Root;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Users\Events\UserCreated;
use App\Http\Controllers\BaseController;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\ValidationRules\NationalRegistryNumber;

class UsersController extends BaseController
{
    protected $modelClass = User::class;

    protected $viewPrefix = 'Root/';

    public function create(Request $request)
    {
        return parent::create($request)
            ->with('roles', Role::all());
    }

    public function edit(Request $request)
    {
        return parent::edit($request)
            ->with('roles', Role::all());
    }

    protected function transformModel(Model $model)
    {
        return [
            'id' => $model->id,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'email' => $model->email,
            'street' => $model->street,
            'number' => $model->number,
            'box' => $model->box,
            'postcode' => $model->postcode,
            'city' => $model->city,
            'country_id' => $model->country_id,
            'national_registry_number' => $model->national_registry_number,
            'birth_date' => $model->birth_date,
            'role_id' => $model->roles()->pluck('id'),
        ];
    }

    protected function createFromRequest($model, Request $request)
    {
        // Update model with current date to skip the part with e-mail validation
        $model->email_verified_at = now();

        $this->updateModel($model, $request);
        $model->syncRoles($request->input('role_id', []));

        event(new UserCreated($model));
    }

    /**
     * @param User $model
     * @param Request $request
     */
    protected function updateFromRequest($model, Request $request)
    {
        $this->updateModel($model, $request);
        $model->syncRoles($request->input('role_id', []));
    }

    protected function all()
    {
        return call_user_func([$this->modelClass, 'orderBy'], 'first_name')
            ->filter(request()->only('search', 'trashed'))
            ->paginate();
    }

    protected function transformAll(LengthAwarePaginator $paginator)
    {
        return $paginator->transform(function (User $user) {
            return [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'avatar' => $user->avatar,
                'verified' => ! ! $user->email_verified_at,
                'complete' => $user->isComplete(),
            ];
        });
    }

    /**
     * @param User|null $model
     * @return array
     */
    protected function validationRules(Request $request, $model = null): array
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['email', 'required'],
            'national_registry_number' => [new NationalRegistryNumber()],
        ];

        $rules['email'][] = $model && $model->id
            ? Rule::unique('users', 'email')->ignore($model->id)
            : 'unique:users,email';

        return $rules;
    }
}
