<?php

namespace App\Http\Controllers\Root;

use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;

class PasswordUrlController extends Controller
{
    public function store($id)
    {
        $model = User::find($id);

        $token = Password::broker()->createToken($model);

        return ['url' => route('password.reset', $token)];
    }
}
