<?php

namespace App\Http\Controllers\Root;

use Illuminate\Http\Request;
use App\Domain\Support\Models\HelpItem;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\BaseController;
use Illuminate\Pagination\LengthAwarePaginator;

class HelpItemsController extends BaseController
{
    protected $modelClass = HelpItem::class;

    protected $viewPrefix = 'Root/';

    protected function createFromRequest($model, Request $request)
    {
        $this->updateModel($model, $request);
    }

    protected function updateFromRequest(Model $model, Request $request)
    {
        $this->updateModel($model, $request);
    }

    protected function validationRules(Request $request, $model = null): array
    {
        return [
            'name' => 'required',
            'reference' => 'required',
            'value' => 'required',
        ];
    }

    protected function transformAll(LengthAwarePaginator $paginator)
    {
        return $paginator->transform(function ($model) {
            return [
                'id' => $model->id,
                'name' => $model->name,
                'reference' => $model->reference,
            ];
        });
    }

    protected function transformModel(Model $model)
    {
        if ($model->child) {
            $model = $model->child;
        }

        return [
            'id' => $model->id,
            'name' => $model->name,
            'value' => $model->value,
        ];
    }

    protected function all()
    {
        return call_user_func([$this->modelClass, 'sort'], request()->only('order_by', 'order_by_dir'))
            ->filter(request()->only('search', 'trashed', 'status'))
            ->where('city_id', null)
            ->paginate();
    }
}
