<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Domain\Users\Enums\Role;
use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use Spatie\Permission\PermissionRegistrar;

class ImpersonateUserController extends Controller
{
    public function update($id, Request $request)
    {
        $user = User::find($id);

        if ($user->hasRole(Role::root()->getValue())) {
            throw new \Exception('Can\'t impersonate root user');
        }

        $request->user()->setImpersonating($id);

        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();

        return redirect()->to($this->redirectTo(), 303);
    }

    public function destroy(Request $request)
    {
        $request->user()->stopImpersonating();

        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();

        return redirect()->to($this->redirectTo(), 303);
    }

    protected function redirectTo()
    {
        return route('home');
    }
}
