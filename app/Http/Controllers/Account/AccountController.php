<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Traits\UpdateDates;
use App\Http\Controllers\Traits\UpdateFillable;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Services\ValidationRules\NationalRegistryNumber;

class AccountController
{
    use UpdateFillable;
    use UpdateDates;
    use ValidatesRequests;

    public function index(Request $request)
    {
        return inertia('Account/Account.vue', ['model' => [
            'id' => $request->user()->id,
            'first_name' => $request->user()->first_name,
            'last_name' => $request->user()->last_name,
            'email' => $request->user()->email,
            'street' => $request->user()->street,
            'number' => $request->user()->number,
            'box' => $request->user()->box,
            'postcode' => $request->user()->postcode,
            'city' => $request->user()->city,
            'country_id' => $request->user()->country_id,
            'national_registry_number' => $request->user()->national_registry_number,
            'birth_date' => $request->user()->birth_date,
        ]]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['email', 'required', Rule::unique('users', 'email')->ignore($request->user()->id)],
            'street' => ['required', 'min:2'],
            'number' => ['required', 'numeric'],
            'postcode' => ['required'],
            'city' => ['required'],
            'country_id' => ['required'],
            'birth_date' => ['required', 'date_format:Y-m-d H:i'],
            'national_registry_number' => [new NationalRegistryNumber()],
        ]);

        $user = $request->user();

        $this->updateFillable($user, $request);
        $this->updateDates($user, $request);

        $user->save();

        return redirect()
            ->back()
            ->with('success', __('Profiel bewaard'));
    }
}
