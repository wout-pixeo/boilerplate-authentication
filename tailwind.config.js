module.exports = {
    theme: {
        colors: {
            transparent: 'transparent',
            black: '#000',
            white: '#fff',

            purple: {
                900: '#21114A',
                800: '#30196C',
                700: '#3F218D',
                600: '#4826A1',
                500: '#6D45D4',
                400: '#8D6EDD',
                300: '#C0ABF7',
                200: '#DBD2F4',
                100: '#EFEAFA',

                transparent: 'rgba(44, 82, 130, 0.9)',
            },

            gray: {
                transparent: 'rgba(160, 174, 192, 0.5)',

                100: '#f7fafc',
                200: '#edf2f7',
                300: '#e2e8f0',
                400: '#cbd5e0',
                500: '#a0aec0',
                600: '#718096',
                700: '#4a5568',
                800: '#2d3748',
                900: '#1a202c',
            },

            blue: {
                100: '#ebf8ff',
                200: '#bee3f8',
                300: '#90cdf4',
                400: '#63b3ed',
                500: '#4299e1',
                600: '#3182ce',
                700: '#2b6cb0',
                800: '#2c5282',
                900: '#2a4365',
            },

            red: {
                900: '#540D13',
                800: '#73111B',
                700: '#9F1825',
                600: '#DF2B3B',
                500: '#E86874',
                400: '#EF959D',
                300: '#F6C1C6',
                200: '#F9D7DA',
                100: '#FBE4E7',
            },

            green: {
                900: '#0A3420',
                800: '#0E4E30',
                700: '#157548',
                600: '#1EA465',
                500: '#5CE1A3',
                400: '#93EBC2',
                300: '#BAF2D8',
                200: '#CDF9E4',
                100: '#E1F9EE',
            },

            orange: {
                900: '#3C2C02',
                800: '#5A4302',
                700: '#C79405',
                600: '#EFB206',
                500: '#FBD362',
                400: '#FBD362',
                300: '#FCE39C',
                200: '#FDEEC3',
                100: '#FEF7E1',
            },
        },

        fontFamily: {
            sans: [
                'Inter',
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                '"Noto Sans"',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
                '"Noto Color Emoji"',
            ],
        },

        borderColor: (theme) => ({
            ...Object.assign(theme('colors'), { default: theme('colors.gray.300', 'currentColor') }),
        }),

        extend: {
            height: {
                '4px': '4px',
            },

            maxHeight: {
                16: '4rem',
                24: '6rem',
            },

            minWidth: {
                xs: '16rem',
                sm: '20rem',
                md: '28rem',
            },

            minHeight: {
                16: '4rem',
            },

            maxWidth: {
                24: '6rem',
            },

            transitionProperty: {
                width: 'width',
            },
        },

        customForms: (theme) => ({
            default: {
                input: {
                    borderColor: theme('colors.gray.400'),
                    fontSize: 'inherit',
                },
                textarea: {
                    borderColor: theme('colors.gray.400'),
                },
                select: {
                    borderColor: theme('colors.gray.400'),
                    fontSize: 'inherit',
                },
                checkbox: {
                    color: theme('colors.purple.500'),
                },
                radio: {
                    color: theme('colors.purple.500'),
                },
            },
        }),
    },

    variants: {
        textColor: ['responsive', 'hover', 'focus', 'group-hover'],
        display: ['responsive', 'hover', 'group-hover'],
        borderWidth: ['responsive', 'last', 'hover', 'focus'],
    },

    plugins: [
        // eslint-disable-next-line import/no-extraneous-dependencies
        require('@tailwindcss/custom-forms'),
    ],
};
