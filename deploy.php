<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/rsync.php';

// Project name
set('application', 'flowlab');

// Project repository
set('repository', 'git@gitlab.com:pixeo/flowlab/flowlab.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
// Laravel shared dirs
set('shared_dirs', [
    'storage',
]);

// Laravel shared file
set('shared_files', [
    '.env',
]);

// Laravel writable dirs
set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);

set('allow_anonymous_stats', false);

set('bin/npm', function () {
    return run('which npm');
});

set('rsync_src', '/tmp/flowlab');
set('tmp_dir', '/tmp');

set('rsync', [
    'exclude'      => ['*'],
    'exclude-file' => false,
    'include'      => [
        'public*/',
        'public*/mix-manifest.json',
        'public*/js/***',
        'public*/css/***',
    ],
    'include-file' => false,
    'filter'       => [],
    'filter-file'  => false,
    'filter-perdir'=> false,
    'flags'        => 'avz', // Recursive, with compress
    'options'      => [],
    'timeout'      => 60,
]);

host('testing')
    ->hostname('174.138.11.154')
    ->forwardAgent(false)
    ->user('forge')
    ->port(22)
    ->stage('testing')
    ->set('branch', 'develop')
    ->set('deploy_path', '/home/forge/flowlab.wonderwoman.pixeo.be');


desc('Deploy your project');
task('deploy', [
    'local:cleanup',
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'local:build',
    'rsync',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:restart_php',
    'cleanup',
    'local:cleanup',
    'success',
]);

desc('Build application');
task('local:build', function () {
    $git = get('bin/git');
    $branch = get('branch');
    $recursive = get('git_recursive', true) ? '--recursive' : '';
    $quiet = isQuiet() ? '-q' : '';
    $options = [
        'tty' => get('git_tty', false),
    ];
    $gitCache = get('git_cache');
    $depth = $gitCache ? '' : '--depth 1';

    $at = '';
    if (! empty($branch)) {
        $at = "-b $branch";
    }

    // If option `tag` is set
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (! empty($tag)) {
            $at = "-b $tag";
        }
    }

    // If option `tag` is not set and option `revision` is set
    if (empty($tag) && input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (! empty($revision)) {
            $depth = '';
        }
    }

    run("$git clone $at $depth $recursive $quiet {{repository}} {{tmp_dir}}/{{application}} 2>&1", $options);

    writeLn("<info>📦  Running NPM</info>");
    run("cd {{tmp_dir}}/{{application}} && {{bin/npm}} install");

    writeLn("<info>🌅  Generating assets</info>");
    run("cd {{tmp_dir}}/{{application}} && {{bin/npm}} run production");
})->local();

task('deploy:restart_php', function () {
    run('sudo -S service php7.2-fpm reload');
});

task('local:cleanup', function () {
    run("cd {{tmp_dir}} && if [ -d {{application}} ]; then rm -Rf {{application}}; fi");
})->local();
