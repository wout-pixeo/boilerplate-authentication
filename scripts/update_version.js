module.exports.readVersion = (contents) => {
    const regex = /define\('FLOWLAB_VERSION', '(\d+\.\d+\.\d+)'\);/gi;

    return regex.exec(contents)[1];
};

module.exports.writeVersion = (contents, version) => {
    // putenv("FLOWLAB_VERSION=5.0.2");
    const regex = /define\('FLOWLAB_VERSION', '(\d+\.\d+\.\d+)'\);/gi;
    return contents.replace(regex, `define('FLOWLAB_VERSION', '${version}');`);
};
