/* eslint-disable import/no-extraneous-dependencies,global-require */
const mix = require('laravel-mix');
const CKEditorWebpackPlugin = require('@ckeditor/ckeditor5-dev-webpack-plugin');
const { styles } = require('@ckeditor/ckeditor5-dev-utils');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig(() => ({
    output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },

    plugins: [
        new CKEditorWebpackPlugin({
            // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
            language: 'nl',
        }),
    ],

    module: {
        rules: [
            {
                // Or /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/ if you want to limit this loader
                // to CKEditor 5 icons only.
                test: /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/,
                use: ['raw-loader'],
            },
            {
                // Or /ckeditor5-[^/]+\/theme\/[\w-/]+\.css$/ if you want to limit this loader
                // to CKEditor 5 theme only.
                test: /ckeditor5-[^/]+\/theme\/[\w-/]+\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            singleton: true,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: styles.getPostCssConfig({
                            themeImporter: {
                                themePath: require.resolve('@ckeditor/ckeditor5-theme-lark'),
                            },
                            minify: true,
                        }),
                    },
                ],
            },
            {
                // Matches all PHP or JSON files in `resources/lang` directory.
                test: /resources[\\/]lang.+\.(php|json)$/,
                loader: 'laravel-localization-loader',
            },
            {
                test: /.(js|vue)$/,
                enforce: 'pre',
                exclude: /node_modules/,
                loader: 'eslint-loader',
            },
            {
                test: /.js$/,
                enforce: 'pre',
                exclude: /node_modules/,
                loader: 'eslint-loader',
            },
        ],
    },
}));


mix
    .copy('resources/fonts', 'public/css/fonts')
    .js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.pcss', 'public/css', [
        require('postcss-easy-import')(),
        require('tailwindcss')('./tailwind.config.js'),
        require('postcss-nested')(),
        require('postcss-preset-env')({
            // The stage option determines which CSS features to polyfill,
            // based upon their stability in the process of becoming
            // implemented web standards.
            // The stage can be 0 (experimental) through 4 (stable), or false.
            // Setting stage to false will disable every polyfill. Doing
            // this would only be useful if you intended to exclusively
            // use the features option
            stage: 0,
        }),
    ])
    .options({
        // Since we don't do any image preprocessing and write url's that are
        // relative to the site root, we don't want the css loader to try to
        // follow paths in `url()` functions.
        processCssUrls: false,
    })
    .version();
