module.exports = {
    root: true,
    parser: 'vue-eslint-parser',
    parserOptions: {
        parser: 'babel-eslint',
        sourceType: 'module',
    },
    env: {
        browser: true,
        node: true,
    },
    extends: ['airbnb-base', 'plugin:vue/recommended'],
    plugins: ['vue', 'import'],
    rules: {
        'max-len': 0,
        'vue/no-template-key': 0,
        indent: ['error', 4, { ignoredNodes: ['TemplateLiteral'] }],
        quotes: [2, 'single'],
        'no-param-reassign': ['error', { props: false }],
        'linebreak-style': 0,
        'vue/html-indent': ['warn', 4],
        'vue/no-v-html': 0,
        'vue/no-template-shadow': 0,
        'vue/require-prop-types': 0,
        'vue/require-default-prop': 0,
    },
    globals: {}
};
