<?php

namespace Tests\Unit\Services\ValidationRules;

use PHPUnit\Framework\TestCase;
use App\Services\ValidationRules\NationalRegistryNumber;

class NationalRegisteryNumberTest extends TestCase
{
    /** @test */
    public function it_can_validate_a_correct_national_registry_number()
    {
        // 18/5/1993
        $this->assertTrue((new NationalRegistryNumber())->passes(null, '93051822361'));
        // 20/11/2000
        $this->assertTrue((new NationalRegistryNumber())->passes(null, '00112000187'));
    }

    /** @test */
    public function it_can_validate_an_incorrect_national_registry_number()
    {
        $this->assertFalse((new NationalRegistryNumber())->passes(null, 'foobar'));
        $this->assertFalse((new NationalRegistryNumber())->passes(null, '93051822362'));
    }
}
