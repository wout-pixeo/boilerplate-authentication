<?php

namespace Tests;

use File;
use Illuminate\Support\Arr;
use PHPUnit\Framework\Assert;
use App\Domain\Users\Models\User;
use App\Domain\Cities\Models\City;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication {
        createApplication as public parentCreateApplication;
    }

    protected function setUp(): void
    {
        parent::setUp();

        TestResponse::macro('props', function ($key = null) {
            $props = json_decode(json_encode($this->original->getData()['page']['props']), JSON_OBJECT_AS_ARRAY);

            return $key
                ? Arr::get($props, $key)
                : $props;
        });

        TestResponse::macro('assertHasProp', function ($key) {
            Assert::assertTrue(Arr::has($this->props(), $key));

            return $this;
        });

        TestResponse::macro('assertPropValue', function ($key, $value) {
            $this->assertHasProp($key);

            if (is_callable($value)) {
                $value($this->props($key));
            } else {
                Assert::assertEquals($this->props($key), $value);
            }

            return $this;
        });

        TestResponse::macro('assertPropCount', function ($key, $count) {
            $this->assertHasProp($key);
            Assert::assertCount($count, $this->props($key));

            return $this;
        });
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = $this->parentCreateApplication();

        $this->getEnvironmentSetup($app);

        $this->setUpTempTestFiles();

        return $app;
    }

    public function getTempDirectory($suffix = '')
    {
        return __DIR__.'/temp'.($suffix == '' ? '' : '/'.$suffix);
    }

    public function getFixtureDirectory($suffix = '')
    {
        return __DIR__.'/fixtures'.($suffix == '' ? '' : '/'.$suffix);
    }

    public function getMediaDirectory($suffix = '')
    {
        return $this->getTempDirectory().'/media'.($suffix == '' ? '' : '/'.$suffix);
    }

    public function getTestFilesDirectory($suffix = '')
    {
        return $this->getTempDirectory().'/testfiles'.($suffix == '' ? '' : '/'.$suffix);
    }

    public function getTestJpg()
    {
        return $this->getTestFilesDirectory('test.jpg');
    }

    protected function getEnvironmentSetup($app)
    {
        $this->initializeDirectory($this->getTempDirectory());

        File::copyDirectory(__DIR__.'/testfiles', $this->getTestFilesDirectory());

        $app['config']->set('filesystems.disks.tmp', [
            'driver' => 'local',
            'root' => $this->getTempDirectory(),
        ]);
    }

    protected function initializeDirectory($directory)
    {
        if (File::isDirectory($directory)) {
            File::deleteDirectory($directory);
        }

        File::makeDirectory($directory);
    }

    protected function setUpTempTestFiles()
    {
        $this->initializeDirectory($this->getTestFilesDirectory());
        File::copyDirectory(__DIR__.'/fixtures/testfiles', $this->getTestFilesDirectory());
    }

    protected function setUpCity()
    {
        $this->city = factory(City::class)->create();
        $this->city->domains()->create([
            'url' => $this->app['config']->get('app.url'),
        ]);

        $this->app->instance('App\CurrentCity', $this->city);
    }

    protected function setupUser(\App\Domain\Users\Enums\Role $role)
    {
        $this->user = factory(User::class)->create();
        $role = Role::create(['name' => $role]);

        $this->user->roles()->attach($role->id);
    }
}
