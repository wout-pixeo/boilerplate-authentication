<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use App\Domain\Cities\Models\City;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Domain\Users\Mail\ResetPassword;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->city = factory(City::class)->create();
        $this->city->domains()->create([
            'url' => $this->app['config']->get('app.url'),
        ]);

        $this->app->instance('App\CurrentCity', $this->city);
    }

    /** @test */
    public function login_form_renders_correctly()
    {
        $this->get(route("login"))->assertSuccessful();
    }

    /** @test */
    public function reset_form_renders_correctly()
    {
        $this->get(route("password.reset", ["token" => "foobar"]))->assertSuccessful();
    }

    /** @test */
    public function forgot_password_form_renders_correctly()
    {
        $this->get(route("password.request"))->assertSuccessful();
    }

    /** @test */
    public function reset_password_email_is_sent()
    {
        // Create user
        $user = factory(User::class)->create();
        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($role->id);

        Mail::fake();

        $this->post(route("password.email"), ['email' => $user->email])->assertRedirect();

        Mail::assertQueued(ResetPassword::class);
    }

    /** @test */
    public function can_see_password_reset_form()
    {
        $user = factory(User::class)->create();
        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($role->id);

        $token = Password::broker()->createToken($user);

        $this
            ->get(route('password.reset', [
                'token' => $token,
            ]))
            ->assertSuccessful();
    }

    /** @test */
    public function can_submit_password_reset()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('secret'),
        ]);
        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($role->id);

        $token = Password::broker()->createToken($user);

        $password = str_random();

        $this
            ->post(route('password.reset.store'), [
                'token' => $token,
                'email' => $user->email,
                'password' => $password,
                'password_confirmation' => $password,
            ])
            ->assertRedirect(route('dashboard.index'));

        $user->refresh();

        $this->assertFalse(Hash::check('secret', $user->password));
        $this->assertTrue(Hash::check($password, $user->password));
    }
}
