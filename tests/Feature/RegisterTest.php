<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use App\Domain\Cities\Models\City;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Notification;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->city = factory(City::class)->create();
        $this->city->domains()->create([
            'url' => $this->app['config']->get('app.url'),
        ]);

        $this->app->instance('App\CurrentCity', $this->city);

        Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
    }

    /** @test */
    public function can_show_register_form()
    {
        $this->get(route('register'))->assertSuccessful();
    }

    /** @test */
    public function user_can_register_and_receive_email()
    {
        Notification::fake();

        $dummy = factory(User::class)->make();

        $this
            ->post(route('register.store'), [
                'first_name' => $dummy->first_name,
                'last_name' => $dummy->last_name,
                'email' => $dummy->email,
                'password' => 'password',
                'password_confirmation' => 'password',
            ]);

        $user = User::where('email', $dummy->email)->first();

        Notification::assertSentTo($user, VerifyEmail::class);
    }
}
