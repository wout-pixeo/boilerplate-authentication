<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use App\Domain\Cities\Models\City;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->city = factory(City::class)->create();
        $this->city->domains()->create([
            'url' => $this->app['config']->get('app.url'),
        ]);

        $this->app->instance('App\CurrentCity', $this->city);

        $this->user = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);

        $this->user->roles()->attach($adminRole->id);

        $this->module = factory(Module::class)->create(['city_id' => $this->city->id]);
    }

    /** @test */
    public function can_view_forms()
    {
        /** @var Module $module */
        $module = factory(Module::class)->create();
        $module->city()->associate($this->city)->save();

        factory(Form::class, 5)->create([
            'city_id' => $this->city->id,
            'module_id' => $module->id,
        ]);

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('api.forms.index', ['module_id' => $module->id]))
            ->assertStatus(200)
            ->assertJsonCount(5);
    }
}
