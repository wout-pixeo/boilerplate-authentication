<?php

namespace Tests\Feature\Api\Locations;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostInfoTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function it_can_look_up_post_info()
    {
        $this->actingAs($this->user)
            ->get(route('api.locations.postinfo.index', ['city' => 'Arendonk', 'search' => '2370']))
            ->assertJson([['id' => '2370', 'name' => '2370']]);
    }
}
