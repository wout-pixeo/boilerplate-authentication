<?php

namespace Tests\Feature\Api\Locations;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function it_can_look_up_streets()
    {
        $this->actingAs($this->user)
            ->get(route('api.locations.addresses.index', [
                'city' => 'Arendonk',
                'street' => 'Hoge Mauw',
                'number' => '1350',
                'search' => '1350',
            ]))
            ->assertJson([
                [
                'id' => '3756999',
                'number' => '1350',
                'box' => '',
                'name' => 'Hoge Mauw 1350, 2370 Arendonk',
                ],
                [
                'id' => '5606017',
                'number' => '1350',
                'box' => 'bus1',
                'name' => 'Hoge Mauw 1350 bus bus1, 2370 Arendonk',
                ],
                [
                'id' => '19347834',
                'number' => '1350',
                'box' => '1',
                'name' => 'Hoge Mauw 1350 bus 1, 2370 Arendonk',
                ],
            ]);
    }
}
