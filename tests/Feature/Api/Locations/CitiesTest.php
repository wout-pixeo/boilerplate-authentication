<?php

namespace Tests\Feature\Api\Locations;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CitiesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function it_can_look_up_cities()
    {
        $this->actingAs($this->user)
            ->get(route('api.locations.cities.index', ['search' => 'Arendonk']))
            ->assertJson([['id' => '13001', 'name' => 'Arendonk']]);
    }
}
