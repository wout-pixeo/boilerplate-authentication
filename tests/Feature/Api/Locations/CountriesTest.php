<?php

namespace Tests\Feature\Api\Locations;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountriesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function it_can_look_up_countries()
    {
        $this->actingAs($this->user)
            ->get(route('api.locations.countries.index'))
            ->assertJsonCount(2);
    }
}
