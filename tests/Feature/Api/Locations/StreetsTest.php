<?php

namespace Tests\Feature\Api\Locations;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StreetsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function it_can_look_up_streets()
    {
        $this->actingAs($this->user)
            ->get(route('api.locations.streets.index', ['city' => 'Arendonk', 'search' => 'Hoge Mauw']))
            ->assertJson([['id' => '12465', 'name' => 'Hoge Mauw']]);
    }
}
