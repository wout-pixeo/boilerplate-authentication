<?php

namespace Tests\Feature\Root;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Cities\Models\Domain;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DomainsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();

        $this->user = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);

        $this->user->roles()->attach($adminRole->id);
    }

    /** @test */
    public function can_view_domains()
    {
        factory(Domain::class, 4)->create([
            'city_id' => $this->city->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('root.domains.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'url', 'default', 'city'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function can_add_domain()
    {
        $this->actingAs($this->user)
            ->get(route('root.domains.create'))
            ->assertSuccessful();
    }

    /** @test */
    public function can_create_domain()
    {
        $model = factory(Domain::class)->make([
            'city_id' => $this->city->id,
        ]);

        $this->actingAs($this->user)
            ->post(route('root.domains.store'), $model->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('domains', [
            'url' => $model->url,
            'city_id' => $model->city_id,
            'default' => $model->default,
        ]);
    }

    /** @test */
    public function can_edit_domain()
    {
        $model = factory(Domain::class)->create([
            'city_id' => $this->city->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('root.domains.edit', $model->id))
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_domain()
    {
        $model = factory(Domain::class)->create(['city_id' => $this->city->id]);
        $modelB = factory(Domain::class)->make(['city_id' => $this->city->id]);

        $this->actingAs($this->user)
            ->put(route('root.domains.update', $model->id), $modelB->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('domains', [
            'url' => $modelB->url,
            'city_id' => $modelB->city_id,
            'default' => $modelB->default,
        ]);
    }

    /** @test */
    public function can_delete_domain()
    {
        $model = factory(Domain::class)->create(['city_id' => $this->city->id]);

        $this->actingAs($this->user)
            ->delete(route('root.domains.destroy', $model->id))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertTrue($model->refresh()->trashed());
    }

    /** @test */
    public function domain_is_only_visible_for_root_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('root.domains.index'))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('root.domains.index'))->assertStatus(403);
    }

    /** @test */
    public function domain_can_not_only_be_edited_by_other_user()
    {
        $model = factory(Domain::class)->create(['city_id' => $this->city->id]);

        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('root.domains.edit', $model->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('root.domains.edit', $model->id))->assertStatus(403);
    }

    /** @test */
    public function domain_can_only_be_deleted_by_root_user()
    {
        $model = factory(Domain::class)->create(['city_id' => $this->city->id]);

        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->delete(route('root.domains.destroy', $model->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->delete(route('root.domains.destroy', $model->id))->assertStatus(403);
    }

    /** @test */
    public function can_search_for_domains()
    {
        factory(Domain::class, 5)
            ->create(['city_id' => $this->city->id])
            ->first()
            ->update(['url' => 'https://www.gemeentemol.be']);

        $this->actingAs($this->user)
            ->get(route('root.domains.index', ['search' => 'https://www.gemeentemol.be']))
            ->assertStatus(200)
            ->assertPropValue('filters.search', 'https://www.gemeentemol.be')
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals('https://www.gemeentemol.be', $models[0]['url']);
            });
    }

    /** @test */
    public function cannot_view_deleted_domains()
    {
        factory(Domain::class, 4)
            ->create(['city_id' => $this->city->id])
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('root.domains.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4);
    }

    /** @test */
    public function can_filter_to_view_deleted_domains()
    {
        factory(Domain::class, 4)
            ->create(['city_id' => $this->city->id])
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('root.domains.index', ['trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 5);
    }
}
