<?php

namespace Tests\Feature\Root;

use Mockery as m;
use Tests\TestCase;
use App\Domain\Users\Models\User;
use Illuminate\Http\UploadedFile;
use Postmark\PostmarkAdminClient;
use App\Domain\Cities\Models\City;
use App\Domain\Media\Models\Media;
use Spatie\Permission\Models\Role;
use App\Domain\Media\Actions\UploadFile;
use Postmark\Models\DynamicResponseModel;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CitiesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->city = factory(City::class)->create();
        $this->city->domains()->create([
            'url' => $this->app['config']->get('app.url'),
        ]);

        $this->app->instance('App\CurrentCity', $this->city);

        $this->user = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);

        $this->user->roles()->attach($adminRole->id);

        $client = m::mock(PostmarkAdminClient::class);
        $client->shouldReceive('getSenderSignature')
            ->with(1)
            ->andReturn(new DynamicResponseModel([
                'Domain' => 'example.com',
                'EmailAddress' => 'john.doe@example.com',
                'ReplyToEmailAddress' => 'reply@example.com',
                'Name' => 'John Doe',
                'Confirmed' => false,
                'SPFVerified' => false,
                'SPFHost' => 'example.com',
                'SPFTextValue' => 'v=spf1 a mx include:spf.mtasv.net ~all',
                'DKIMVerified' => false,
                'WeakDKIM' => false,
                'DKIMHost' => '',
                'DKIMTextValue' => '',
                'DKIMPendingHost' => '20140220130148.pm._domainkey.example.com',
                'DKIMPendingTextValue' => 'k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQ35xZciGB0g...',
                'DKIMRevokedHost' => '',
                'DKIMRevokedTextValue' => '',
                'SafeToRemoveRevokedKeyFromDNS' => false,
                'DKIMUpdateStatus' => 'Pending',
                'ReturnPathDomain' => 'pmbounces.wildbit.com',
                'ReturnPathDomainVerified' => true,
                'ReturnPathDomainCNAMEValue' => 'pm.mtasv.net',
                'ID' => 1,
            ]));

        $this->app->instance(PostmarkAdminClient::class, $client);
    }

    /** @test */
    public function can_view_cities()
    {
        factory(City::class, 5)->create();

        $this->actingAs($this->user)
            ->get(route('root.cities.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 6)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function can_add_city()
    {
        $model = factory(City::class)->make();

        $upload = (new UploadFile())->execute(new UploadedFile($this->getTestJpg(), 'test.jpg'));

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('root.cities.store'), array_merge($model->toArray(), [
                'logo_heading' => [$upload->toArray()],
            ]))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('cities', [
            'name' => $model->name,
            'street' => $model->street,
            'number' => $model->number,
            'box' => $model->box,
            'postcode' => $model->postcode,
            'city' => $model->city,
            'country_id' => $model->country_id,
            'email' => $model->email,
            'phone' => $model->phone,
            'website' => $model->website,
            'intro' => $model->intro,
            'footer' => $model->footer,
            'privacy_link' => $model->privacy_link,
            'disclaimer_link' => $model->disclaimer_link,
        ]);

        $media = Media::where('file_name', 'test.jpg')->first();
        $this->assertEquals(1, $media->id);
        $this->assertEquals('logo_heading', $media->collection_name);
    }

    /** @test */
    public function can_edit_city()
    {
        $model = factory(City::class)->create();

        $this->actingAs($this->user)
            ->get(route('root.cities.edit', $model->id))
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_city()
    {
        $model = factory(City::class)->create();
        $modelB = factory(City::class)->make();

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(route('root.cities.update', $model->id), $modelB->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('cities', [
            'name' => $modelB->name,
            'street' => $modelB->street,
            'number' => $modelB->number,
            'box' => $modelB->box,
            'postcode' => $modelB->postcode,
            'city' => $modelB->city,
            'country_id' => $modelB->country_id,
            'email' => $modelB->email,
            'phone' => $modelB->phone,
            'website' => $modelB->website,
            'intro' => $modelB->intro,
            'footer' => $modelB->footer,
            'privacy_link' => $modelB->privacy_link,
            'disclaimer_link' => $modelB->disclaimer_link,
        ]);
    }

    /** @test */
    public function can_remove_image()
    {
        $model = factory(City::class)->create();
        $model->addMedia($this->getTestJpg())->toMediaCollection('logo_heading', 'local');

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(route('root.cities.update', $model->id), array_merge($model->toArray(), ['logo_heading' => []]))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $model->refresh();
        $this->assertEmpty($model->logo_heading);
    }

    /** @test */
    public function can_delete_city()
    {
        /** @var City $model */
        $model = factory(City::class)->create();

        $this->actingAs($this->user)
            ->delete(route('root.cities.destroy', $model->id))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertTrue($model->refresh()->trashed());
    }

    /** @test */
    public function city_is_only_visible_for_root_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('root.cities.index'))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('root.cities.index'))->assertStatus(403);
    }

    /** @test */
    public function city_can_not_only_be_edited_by_other_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('root.cities.edit', $this->city->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('root.cities.edit', $this->city->id))->assertStatus(403);
    }

    /** @test */
    public function city_can_only_be_deleted_by_root_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->delete(route('root.cities.destroy', $this->city->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->delete(route('root.cities.destroy', $this->city->id))->assertStatus(403);
    }

    /** @test */
    public function can_search_for_cities()
    {
        factory(City::class, 5)
        ->create()
        ->first()
        ->update(['name' => 'Gemeente Mol']);

        $this->actingAs($this->user)
            ->get(route('root.cities.index', ['search' => 'Gemeente Mol']))
            ->assertStatus(200)
            ->assertPropValue('filters.search', 'Gemeente Mol')
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals('Gemeente Mol', $models[0]['name']);
            });
    }

    /** @test */
    public function cannot_view_deleted_cities()
    {
        factory(City::class, 4)
        ->create()
        ->first()
        ->delete();

        $this->actingAs($this->user)
            ->get(route('root.cities.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4);
    }

    /** @test */
    public function can_filter_to_view_deleted_cities()
    {
        factory(City::class, 4)
            ->create()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('root.cities.index', ['trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 5);
    }
}
