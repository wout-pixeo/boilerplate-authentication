<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompleteAccountTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
    }

    /** @test */
    public function incomplete_user_is_sent_to_account_complete_form()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('secret'),
            'street' => null,
        ]);
        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($role->id);

        $this->actingAs($user)
            ->get(route('dashboard.index'))
            ->assertRedirect(route('auth.complete'));

        $this->get(route('auth.complete'))->assertSuccessful();
    }

    /** @test */
    public function can_submit_user_complete_form()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('secret'),
            'street' => null,
        ]);

        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($role->id);

        $this->withoutExceptionHandling()->actingAs($user)
            ->post(route('auth.complete.store'), [
                'first_name' => 'Nick',
                'last_name' => 'Goris',
                'street' => 'Hoge Mauw',
                'number' => '1351',
                'postcode' => '2370',
                'city' => 'Arendonk',
                'country_id' => 1,
                'birth_date' => '1985-11-20 00:00',
                'national_registry_number' => '00112000187',
            ])
        ->assertRedirect(route('dashboard.index'));
    }
}
