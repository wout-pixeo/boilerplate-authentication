<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Modules\Models\Module;
use App\Domain\Modules\Models\Question;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderQuestionsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
        $this->module = factory(Module::class)->create(['city_id' => $this->city->id]);
    }

    /** @test */
    public function it_can_order_questions()
    {
        $questions = factory(Question::class, 5)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->put(
                route('admin.questions.order.index', ['module' => $this->module->slug]),
                ['id' => $questions->pluck('id')->reverse()->toArray()]
            )
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertEquals(5, $questions->get(0)->refresh()->order);
        $this->assertEquals(4, $questions->get(1)->refresh()->order);
        $this->assertEquals(3, $questions->get(2)->refresh()->order);
        $this->assertEquals(2, $questions->get(3)->refresh()->order);
        $this->assertEquals(1, $questions->get(4)->refresh()->order);
    }
}
