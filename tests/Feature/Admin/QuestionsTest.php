<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Module;
use App\Domain\Modules\Models\Question;
use App\Domain\Modules\Models\QuestionRule;
use App\Domain\Modules\Models\QuestionAnswer;
use App\Domain\Modules\Enums\QuestionRuleOperator;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());

        $this->module = factory(Module::class)->create(['city_id' => $this->city->id]);
    }

    /** @test */
    public function can_view_questions_from_same_module()
    {
        /** @var Module $module */
        $module = factory(Module::class)->create();
        $module->city()->associate($this->city)->save();

        factory(Question::class, 5)->create([
            'city_id' => $this->city->id,
            'module_id' => $module->id,
        ]);

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('admin.questions.index', ['module' => $module->slug]))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name', 'description', 'type', 'precision', 'order'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function cant_view_questions_from_other_modules()
    {
        $models = factory(Module::class, 5)->create(['city_id' => $this->city->id])
            ->map(function ($module) {
                return factory(Question::class)->create([
                    'module_id' => $module->id,
                    'city_id' => $module->city_id,
                ]);
            });

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('admin.questions.index', ['module' => $models->first()->module->slug]))
            ->assertStatus(200)
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name', 'description', 'type', 'precision', 'order'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function can_add_question_and_answers()
    {
        $model = factory(Question::class)->make();
        $answers = factory(QuestionAnswer::class, 5)->make();

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('admin.questions.store', ['module' => $this->module->slug]), array_merge($model->toArray(), [
                'answers' => $answers->toArray(),
            ]))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('questions', [
            'name' => $model->name,
            'description' => $model->description,
            'type' => $model->type,
        ]);

        $this->assertCount(5, QuestionAnswer::all());
    }

    /** @test */
    public function can_edit_question()
    {
        $model = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('admin.questions.edit', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_question()
    {
        $model = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $answers = factory(QuestionAnswer::class, 5)->create(['question_id' => $model->id]);

        $new = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $newAnswers = $answers->take(1)->push(factory(QuestionAnswer::class)->make());

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(route('admin.questions.update', ['id' => $model->id, 'module' => $this->module->slug]), array_merge($new->toArray(), [
                'answers' => $newAnswers->toArray(),
            ]))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('questions', [
            'name' => $new->name,
            'description' => $new->description,
            'type' => $new->type,
        ]);

        $this->assertCount(2, QuestionAnswer::all());
    }

    /** @test */
    public function can_update_question_rules()
    {
        $model = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $parentQuestion = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $parentQuestionAnswer = factory(QuestionAnswer::class)->create([
            'question_id' => $parentQuestion->id,
            'default' => true,
        ]);

        $rules = factory(QuestionRule::class, 5)
            ->create([
                'question_id' => $model->id,
                'target_question_id' => factory(Question::class)->create([
                    'city_id' => $this->city->id,
                    'module_id' => $this->module->id,
                ]),
                'operator' => QuestionRuleOperator::equals()->getValue(),
                'question_answer_id' => $parentQuestionAnswer->id,
            ]);

        $new = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $newAnswers = $rules->take(1)->push(factory(QuestionRule::class)
            ->make([
                'target_question_id' => factory(Question::class)->create([
                    'city_id' => $this->city->id,
                    'module_id' => $this->module->id,
                ]),
                'operator' => QuestionRuleOperator::equals()->getValue(),
                'question_answer_id' => $parentQuestionAnswer->id,
            ]));

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(route('admin.questions.update', ['id' => $model->id, 'module' => $this->module->slug]), array_merge($new->toArray(), [
                'rules' => $newAnswers->toArray(),
            ]))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('questions', [
            'name' => $new->name,
            'description' => $new->description,
            'type' => $new->type,
        ]);

        $this->assertCount(2, QuestionRule::all());
    }

    /** @test */
    public function question_can_only_be_deleted_by_admin_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $model = factory(Question::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($admin)
            ->delete(route('admin.questions.destroy', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)
            ->delete(route('admin.questions.destroy', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(403);
    }

    /** @test */
    public function can_search_for_questions()
    {
        factory(Question::class, 5)
            ->create([
                'city_id' => $this->city->id,
                'module_id' => $this->module->id,
            ])
            ->first()
            ->update(['name' => 'Ben je zelfstandig?']);

        $this->actingAs($this->user)
            ->get(route('admin.questions.index', ['module' => $this->module->slug, 'search' => 'Ben je zelfstandig?']))
            ->assertStatus(200)
            ->assertPropValue('filters.search', 'Ben je zelfstandig?')
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals('Ben je zelfstandig?', $models[0]['name']);
            });
    }

    /** @test */
    public function cannot_view_deleted_questions()
    {
        factory(Question::class, 5)
            ->create([
                'city_id' => $this->city->id,
                'module_id' => $this->module->id,
            ])
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.questions.index', ['module' => $this->module->slug]))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4);
    }

    /** @test */
    public function can_filter_to_view_deleted_questions()
    {
        factory(Question::class, 5)
            ->create([
                'city_id' => $this->city->id,
                'module_id' => $this->module->id,
            ])
            ->first()
            ->delete();


        $this->actingAs($this->user)
            ->get(route('admin.questions.index', ['module' => $this->module->slug, 'trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 5);
    }
}
