<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use App\Domain\Modules\Enums\FormStatus;
use App\Domain\Modules\Models\ModuleItem;
use App\Domain\Workflows\Models\Workflow;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModuleItemFormStatusTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();

        $this->user = factory(User::class)->create();

        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);

        $this->user->roles()->attach($role->id);
        $this->module = factory(Module::class)->create();
        $this->workflow = tap(factory(Workflow::class)->make(), function ($model) {
            $model->city()->associate($this->city);
            $model->typeable()->associate($this->module);
            $model->save();
        });
    }

    /** @test */
    public function it_can_change_the_status_of_a_form()
    {
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);
        $moduleItem = factory(ModuleItem::class)->create([
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
        ]);
        $form->moduleItems()->save($moduleItem, [
            'form_audit_id' => $form->audits()->first()->id,
            'status' => FormStatus::submitted()->getIndex(),
        ]);

        $this->actingAs($this->user)
            ->post(route('admin.moduleItemFormStatus.store', [
                'module' => $this->module->slug,
                'itemId' => $moduleItem->id,
                'id' => $form->id,
            ]), [
                'status' => FormStatus::approved()->getValue(),
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('form_module_item', [
            'form_id' => $form->id,
            'module_item_id' => $moduleItem->id,
            'status' => FormStatus::approved()->getIndex(),
        ]);
    }
}
