<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Support\Models\HelpItem;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpItemsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function can_view_help_items()
    {
        $models = factory(HelpItem::class, 4)->create();
        $new = tap($models->first()->replicate(), function ($model) {
            $model->city_id = $this->city->id;
            $model->save();
        });

        $this->actingAs($this->user)
            ->get(route('admin.helpItems.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name', 'city_id', 'reference'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function can_edit_help_item()
    {
        $model = factory(HelpItem::class)->create();
        $model = $model->replicate();
        $model->name = 'Hello';
        $model->city()->associate($this->city);
        $model->save();
        $model->refresh();

        $this->actingAs($this->user)
            ->get(route('admin.helpItems.edit', $model->id))
            ->assertStatus(200)
            ->assertPropValue('model', function ($model) {
                $this->assertEquals('Hello', $model['name']);
            });
    }

    /** @test */
    public function can_update_help_item()
    {
        $old = factory(HelpItem::class)->create();
        $new = factory(HelpItem::class)->make();

        $this->actingAs($this->user)
            ->put(route('admin.helpItems.update', $old->id), $new->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('help_items', [
            'name' => $new->name,
            'reference' => $new->reference,
            'value' => $new->value,
            'city_id' => $this->city->id,
        ]);
    }

    /** @test */
    public function can_delete_help_item()
    {
        $model = factory(HelpItem::class)->create();

        $this->actingAs($this->user)
            ->delete(route('admin.helpItems.destroy', $model->id))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertTrue($model->refresh()->trashed());
    }

    /** @test */
    public function help_item_is_only_visible_for_admin_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('admin.helpItems.index'))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('admin.helpItems.index'))->assertStatus(403);
    }

    /** @test */
    public function help_item_can_not_only_be_edited_by_other_user()
    {
        $model = factory(HelpItem::class)->create();

        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('admin.helpItems.edit', $model->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('admin.helpItems.edit', $model->id))->assertStatus(403);
    }

    /** @test */
    public function help_item_can_only_be_deleted_by_admin_user()
    {
        $model = factory(HelpItem::class)->create(['city_id' => $this->city->id]);

        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->delete(route('admin.helpItems.destroy', $model->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->delete(route('admin.helpItems.destroy', $model->id))->assertStatus(403);
    }

    /** @test */
    public function can_search_for_help_items()
    {
        factory(HelpItem::class, 5)
            ->create()
            ->first()
            ->update(['name' => 'Instructies']);

        $this->actingAs($this->user)
            ->get(route('admin.helpItems.index', ['search' => 'Instructies']))
            ->assertStatus(200)
            ->assertPropValue('filters.search', 'Instructies')
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals('Instructies', $models[0]['name']);
            });
    }

    /** @test */
    public function cannot_view_deleted_help_items()
    {
        factory(HelpItem::class, 5)
            ->create()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.helpItems.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4);
    }

    /** @test */
    public function can_filter_to_view_deleted_help_items()
    {
        factory(HelpItem::class, 5)
            ->create()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.helpItems.index', ['trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 5);
    }
}
