<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());

        $this->module = factory(Module::class)->create(['city_id' => $this->city->id]);
    }

    /** @test */
    public function can_view_forms_from_same_module()
    {
        /** @var Module $module */
        $module = factory(Module::class)->create();
        $module->city()->associate($this->city)->save();

        factory(Form::class, 5)->create([
            'city_id' => $this->city->id,
            'module_id' => $module->id,
        ]);

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('admin.forms.index', ['module' => $module->slug]))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function cant_view_forms_from_other_modules()
    {
        $models = factory(Module::class, 5)->create(['city_id' => $this->city->id])
            ->map(function ($module) {
                return factory(Form::class)->create([
                    'module_id' => $module->id,
                    'city_id' => $module->city_id,
                ]);
            });

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('admin.forms.index', ['module' => $models->first()->module->slug]))
            ->assertStatus(200)
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function it_can_see_form_create_view()
    {
        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('admin.forms.create', ['module' => $this->module->slug]))
            ->assertStatus(200);
    }

    /** @test */
    public function can_add_form()
    {
        $model = factory(Form::class)->make();

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('admin.forms.store', ['module' => $this->module->slug]), $model->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('forms', [
            'name' => $model->name,
            'contact_name' => $model->contact_name,
            'contact_email' => $model->contact_email,
        ]);
    }

    /** @test */
    public function can_edit_form()
    {
        $model = factory(Form::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('admin.forms.edit', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_form()
    {
        $model = factory(Form::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $new = factory(Form::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(route('admin.forms.update', ['id' => $model->id, 'module' => $this->module->slug]), $new->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('forms', [
            'name' => $new->name,
            'contact_name' => $new->contact_name,
            'contact_email' => $new->contact_email,
        ]);
    }

    /** @test */
    public function form_can_only_be_deleted_by_admin_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $model = factory(Form::class)->create([
            'city_id' => $this->city->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($admin)
            ->delete(route('admin.forms.destroy', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)
            ->delete(route('admin.forms.destroy', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(403);
    }

    /** @test */
    public function can_search_for_forms()
    {
        factory(Form::class, 5)
            ->create([
                'city_id' => $this->city->id,
                'module_id' => $this->module->id,
            ])
            ->first()
            ->update(['name' => 'Formulier']);

        $this->actingAs($this->user)
            ->get(route('admin.forms.index', ['module' => $this->module->slug, 'search' => 'Formulier']))
            ->assertStatus(200)
            ->assertPropValue('filters.search', 'Formulier')
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals('Formulier', $models[0]['name']);
            });
    }

    /** @test */
    public function cannot_view_deleted_forms()
    {
        factory(Form::class, 5)
            ->create([
                'city_id' => $this->city->id,
                'module_id' => $this->module->id,
            ])
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.forms.index', ['module' => $this->module->slug]))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4);
    }

    /** @test */
    public function can_filter_to_view_deleted_forms()
    {
        factory(Form::class, 5)
            ->create([
                'city_id' => $this->city->id,
                'module_id' => $this->module->id,
            ])
            ->first()
            ->delete();


        $this->actingAs($this->user)
            ->get(route('admin.forms.index', ['module' => $this->module->slug, 'trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 5);
    }

    /** @test */
    public function it_can_add_a_file_to_the_downloads_field()
    {
        $this->markTestIncomplete();
    }
}
