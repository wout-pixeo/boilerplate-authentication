<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use App\Domain\Modules\Models\Question;
use App\Domain\Modules\Models\QuestionAnswer;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionAnswersTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
        $this->module = factory(Module::class)->create(['city_id' => $this->city->id]);
    }

    /** @test */
    public function it_can_add_a_form_to_a_question()
    {
        $question = factory(Question::class)->create(['module_id' => $this->module->id]);
        $answer = factory(QuestionAnswer::class)->create(['question_id' => $question->id]);

        $this->actingAs($this->user)
            ->get(route('admin.questionAnswers.index', ['module' => $this->module->slug, 'id' => $answer->id]))
            ->assertSuccessful();
    }

    /** @test */
    public function it_can_associate_a_form_with_a_question()
    {
        $question = factory(Question::class)->create(['module_id' => $this->module->id]);
        $answer = factory(QuestionAnswer::class)->create(['question_id' => $question->id]);
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);

        $this->actingAs($this->user)
            ->post(route('admin.questionAnswers.store', ['module' => $this->module->slug]), [
                'links' => [
                    [
                        'question_id' => $question->id,
                        'answer_id' => $answer->id,
                        'forms' => [
                            ['id' => $form->id, 'required' => false],
                        ],
                    ],
                ],
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('form_question_answer', [
            'form_id' => $form->id,
            'question_answer_id' => $answer->id,
            'required' => 0,
        ]);
    }

    /** @test */
    public function it_can_remove_a_form_from_a_question()
    {
        $question = factory(Question::class)->create(['module_id' => $this->module->id]);
        $answer = factory(QuestionAnswer::class)->create(['question_id' => $question->id]);
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);
        $answer->forms()->sync([$form->id]);

        $this->assertDatabaseHas('form_question_answer', [
            'form_id' => $form->id,
            'question_answer_id' => $answer->id,
            'required' => 0,
        ]);

        $this->actingAs($this->user)
            ->post(route('admin.questionAnswers.store', ['module' => $this->module->slug, 'id' => $answer->id]), [
                'links' => [
                    [
                        'question_id' => $question->id,
                        'answer_id' => $answer->id,
                        'forms' => [],
                    ],
                ],
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseMissing('form_question_answer', [
            'form_id' => $form->id,
            'question_answer_id' => $answer->id,
        ]);
    }

    /** @test */
    public function it_can_mark_a_form_as_required()
    {
        $question = factory(Question::class)->create(['module_id' => $this->module->id]);
        $answer = factory(QuestionAnswer::class)->create(['question_id' => $question->id]);
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);
        $answer->forms()->sync([$form->id]);

        $this->assertDatabaseHas('form_question_answer', [
            'form_id' => $form->id,
            'question_answer_id' => $answer->id,
            'required' => 0,
        ]);

        $this->actingAs($this->user)
            ->post(route('admin.questionAnswers.store', ['module' => $this->module->slug, 'id' => $answer->id]), [
                'links' => [
                    [
                        'question_id' => $question->id,
                        'answer_id' => $answer->id,
                        'forms' => [
                            ['id' => $form->id, 'required' => true],
                        ],
                    ],
                ],
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('form_question_answer', [
            'form_id' => $form->id,
            'question_answer_id' => $answer->id,
            'required' => 1,
        ]);
    }

    /** @test */
    public function link_can_only_be_modified_by_admin_user()
    {
        $question = factory(Question::class)->create(['module_id' => $this->module->id]);
        $answer = factory(QuestionAnswer::class)->create(['question_id' => $question->id]);
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);

        $root = factory(User::class)->create();
        $rootRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $root->roles()->attach($rootRole->id);

        $this->actingAs($root)
            ->post(route('admin.questionAnswers.store', ['module' => $this->module->slug, 'id' => $answer->id]), [
                'links' => [
                    [
                        'id' => $answer->id,
                        'forms' => [
                            ['id' => $form->id, 'required' => true],
                        ],
                    ],
                ],
            ])
            ->assertStatus(403);
    }
}
