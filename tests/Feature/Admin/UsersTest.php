<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function can_view_users()
    {
        factory(User::class, 5)->create();

        $this->actingAs($this->user)
            ->get(route('admin.users.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 6)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'first_name', 'last_name', 'email', 'avatar', 'verified', 'complete'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function can_add_user()
    {
        $this->actingAs($this->user)
            ->get(route('admin.users.create'))
            ->assertSuccessful();
    }

    /** @test */
    public function can_create_user()
    {
        Mail::fake();

        $model = factory(User::class)->make();

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('admin.users.store'), $model->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('users', [
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'email' => $model->email,
            'birth_date' => $model->birth_date,
        ]);
    }

    /** @test */
    public function can_edit_user()
    {
        $model = factory(User::class)->create();

        $this->actingAs($this->user)
            ->get(route('admin.users.edit', $model->id))
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_user()
    {
        $model = factory(User::class)->create();
        $new = factory(User::class)->make();

        $this->actingAs($this->user)
            ->put(route('admin.users.update', $model->id), $new->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('users', $new->toArray());
    }

    /** @test */
    public function can_delete_user()
    {
        /** @var User $model */
        $model = factory(User::class)->create();

        $this->actingAs($this->user)
            ->delete(route('admin.users.destroy', $model->id))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertTrue($model->refresh()->trashed());
    }

    /** @test */
    public function can_search_for_users()
    {
        factory(User::class, 5)
            ->create()
            ->first()
            ->update(['first_name' => 'Jos']);

        $this->actingAs($this->user)
            ->get(route('admin.users.index', ['search' => 'Jos']))
            ->assertStatus(200)
            ->assertPropValue('filters.search', 'Jos')
            ->assertPropCount('models.data', 1)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals('Jos', $models[0]['first_name']);
            });
    }

    /** @test */
    public function cannot_view_deleted_users()
    {
        factory(User::class, 5)
            ->create()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.users.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5);
    }

    /** @test */
    public function can_filter_to_view_deleted_users()
    {
        factory(User::class, 5)
            ->create()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.users.index', ['trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 6);
    }
}
