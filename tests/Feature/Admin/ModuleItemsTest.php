<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Module;
use App\Domain\Modules\Models\ModuleItem;
use App\Domain\Workflows\Models\Workflow;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModuleItemsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();

        $this->user = factory(User::class)->create();

        $role = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);

        $this->user->roles()->attach($role->id);
        $this->module = factory(Module::class)->create();
        $this->workflow = tap(factory(Workflow::class)->make(), function ($model) {
            $model->city()->associate($this->city);
            $model->typeable()->associate($this->module);
            $model->save();
        });
    }

    /** @test */
    public function it_can_view_module_items()
    {
        factory(ModuleItem::class, 5)->create([
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('admin.moduleItems.index', $this->module->slug))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name', 'status', 'created_at'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function it_can_view_module_item_detail()
    {
        $model = factory(ModuleItem::class)->create([
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('admin.moduleItems.show', ['module' => $this->module->slug, 'id' => $model->id]))
            ->assertStatus(200)
            ->assertPropValue('model', function ($model) {
                $this->assertEquals(
                    ['id', 'workflow_items', 'questions', 'forms'],
                    array_keys($model)
                );
            });
    }
}
