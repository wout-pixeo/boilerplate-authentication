<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use App\Domain\Workflows\Models\Workflow;
use App\Domain\Modules\Enums\ModuleItemStatus;
use App\Domain\Workflows\Enums\WorkflowItemType;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorkflowTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
        $this->module = factory(Module::class)->create(['city_id' => $this->city->id]);
    }

    /** @test */
    public function can_view_workflows()
    {
        $models = tap(factory(Workflow::class, 5)->make(), function ($models) {
            $models->each(function ($model) {
                $model->city()->associate($this->city);

                $form = factory(Form::class)->create([
                    'city_id' => $this->city->id,
                    'module_id' => $this->module->id,
                ]);

                $model->typeable()->associate($form);

                $model->save();
            });
        });

        $this->actingAs($this->user)
            ->get(route('admin.workflows.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'name'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function can_edit_workflow()
    {
        /** @var Workflow $model */
        $model = factory(Workflow::class)->make([
            'city_id' => $this->city->id,
        ]);

        $model->typeable()->associate($this->module);

        $model->save();

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('admin.workflows.edit', ['id' => $model->id, 'module' => $this->module->slug]))
            ->assertStatus(200);
    }

    /** @test */
    public function it_can_create_a_workflow_with_default_item_templates()
    {
        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(
                route('admin.workflows.store', ['module' => $this->module->slug]),
                ['type' => ['typeable_type' => get_class($this->module), 'typeable_id' => $this->module->id]]
            )
            ->assertStatus(302);

        $this->assertDatabaseHas('workflows', [
            'typeable_type' => get_class($this->module),
            'typeable_id' => $this->module->id,
        ]);

        $this->assertDatabaseHas('workflow_item_templates', [
            'group' => ModuleItemStatus::created()->getName(),
        ]);

        $this->assertDatabaseHas('workflow_item_templates', [
            'group' => ModuleItemStatus::submitted()->getName(),
        ]);
    }

    /** @test */
    public function can_update_workflow()
    {
        /** @var Workflow $model */
        $model = factory(Workflow::class)->make([
            'city_id' => $this->city->id,
        ]);

        $model->typeable()->associate($this->module);

        $model->save();

        $this->actingAs($this->user)
            ->put(
                route('admin.workflows.update', ['id' => $model->id, 'module' => $this->module->slug]),
                ['items' => [
                    [
                        'type' => WorkflowItemType::todo()->getValue(),
                        'group' => 'foobar',
                        'name' => 'Dit is een taak',
                    ],
                ]]
            )
            ->assertStatus(302);

        $this->assertDatabaseHas('workflow_item_templates', [
            'type' => WorkflowItemType::todo()->getIndex(),
            'group' => 'foobar',
            'name' => 'Dit is een taak',
        ]);
    }

    /** @test */
    public function can_delete_workflow()
    {
        $model = $this->createWorkflow();

        $this->actingAs($this->user)
            ->delete(route('admin.workflows.destroy', $model->id))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertTrue($model->refresh()->trashed());
    }

    /** @test */
    public function workflow_is_only_visible_for_admin_user()
    {
        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('admin.workflows.index'))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('admin.workflows.index'))->assertStatus(403);
    }

    /** @test */
    public function workflow_can_not_only_be_edited_by_other_user()
    {
        $model = $this->createWorkflow();

        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->get(route('admin.workflows.edit', $model->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->get(route('admin.workflows.edit', $model->id))->assertStatus(403);
    }

    /** @test */
    public function workflow_can_only_be_deleted_by_admin_user()
    {
        $model = $this->createWorkflow();

        $admin = factory(User::class)->create();
        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $admin->roles()->attach($adminRole->id);

        $this->actingAs($admin)->delete(route('admin.workflows.destroy', $model->id))->assertStatus(403);

        $user = factory(User::class)->create();
        $userRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $user->roles()->attach($userRole->id);

        $this->actingAs($user)->delete(route('admin.workflows.destroy', $model->id))->assertStatus(403);
    }

    /** @test */
    public function cannot_view_deleted_workflows()
    {
        $this->createWorkflows()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.workflows.index'))
            ->assertStatus(200)
            ->assertPropCount('models.data', 4);
    }

    /** @test */
    public function it_can_filter_to_view_deleted_workflows()
    {
        $this->createWorkflows()
            ->first()
            ->delete();

        $this->actingAs($this->user)
            ->get(route('admin.workflows.index', ['trashed' => 'with']))
            ->assertStatus(200)
            ->assertPropValue('filters.trashed', 'with')
            ->assertPropCount('models.data', 5);
    }

    protected function createWorkflow() : Workflow
    {
        return $this->createWorkflows(1)->first();
    }

    protected function createWorkflows($amount = 5)
    {
        return tap(factory(Workflow::class)->times($amount)->make(), function ($models) {
            $models->each(function ($model) {
                $model->city()->associate($this->city);

                $form = factory(Form::class)->create([
                    'city_id' => $this->city->id,
                    'module_id' => $this->module->id,
                ]);

                $model->typeable()->associate($form);

                $model->save();
            });
        });
    }
}
