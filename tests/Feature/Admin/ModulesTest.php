<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Domain\Modules\Models\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModulesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::admin());
    }

    /** @test */
    public function can_add_module()
    {
        $model = factory(Module::class)->make();

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('admin.modules.store'), $model->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('modules', [
            'name' => $model->name,
            'city_id' => $this->city->id,
        ]);
    }

    /** @test */
    public function can_edit_module()
    {
        $model = factory(Module::class)->create();

        $this->actingAs($this->user)
            ->get(route('admin.modules.edit', $model->id))
            ->assertStatus(200);
    }

    /** @test */
    public function can_update_module()
    {
        $model = factory(Module::class)->create();
        $new = factory(Module::class)->make();

        $this->actingAs($this->user)
            ->put(route('admin.modules.update', $model->id), $new->toArray())
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('modules', $new->toArray());
    }

    /** @test */
    public function can_delete_module()
    {
        /** @var Module $model */
        $model = factory(Module::class)->create();

        $this->actingAs($this->user)
            ->delete(route('admin.modules.destroy', $model->id))
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertTrue($model->refresh()->trashed());
    }
}
