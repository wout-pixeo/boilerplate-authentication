<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
    }

    /** @test */
    public function sends_password_reset_email()
    {
        Mail::fake();

        $user = factory(User::class)->create();

        $response = $this->post(route('password.email'), ['email' => $user->email]);

        Mail::assertQueued(\App\Domain\Users\Mail\ResetPassword::class);

        $response->assertStatus(302);
    }

    /** @test */
    public function does_not_send_password_reset_email()
    {
        $this->doesntExpectJobs(ResetPassword::class);

        $this->post(route('password.email'), ['email' => 'invalid@email.com']);
    }

    /** @test */
    public function displays_password_reset_form()
    {
        $this->get(route('password.reset', ['token' => str_random()]))->assertStatus(200);
    }

    /** @test */
    public function changes_a_users_password()
    {
        $user = factory(User::class)->create();

        $token = Password::createToken($user);

        $this->post(route('password.reset.store'), [
            'token' => $token,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $this->assertTrue(Hash::check('password', $user->fresh()->password));
    }
}
