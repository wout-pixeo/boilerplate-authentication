<?php

namespace Tests\Feature\User;

use Tests\TestCase;
use App\Domain\Users\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::root());
    }

    /** @test */
    public function can_view_profile_form()
    {
        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->get(route('account.index'))
            ->assertSuccessful()
            ->assertPropValue('model', function ($model) {
                $this->assertEquals(
                    [
                        'id', 'first_name', 'last_name', 'email', 'street',
                        'number', 'box', 'postcode', 'city', 'country_id', 'national_registry_number', 'birth_date',
                    ],
                    array_keys($model)
                );
            });
    }

    /** @test */
    public function user_can_update_own_profile()
    {
        $new = factory(User::class)->make();

        $this->actingAs($this->user)
            ->post(route('account.store'), [
                'first_name' => $new->first_name,
                'last_name' => $new->last_name,
                'email' => $new->email,
                'street' => $new->street,
                'number' => $new->number,
                'box' => $new->box,
                'postcode' => $new->postcode,
                'city' => $new->city,
                'country_id' => $new->country_id,
                'national_registry_number' => $new->national_registry_number,
                'birth_date' => $new->birth_date->format('Y-m-d 00:00'),
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('users', [
            'first_name' => $new->first_name,
            'last_name' => $new->last_name,
            'email' => $new->email,
            'street' => $new->street,
            'number' => $new->number,
            'box' => $new->box,
            'postcode' => $new->postcode,
            'city' => $new->city,
            'country_id' => $new->country_id,
            'national_registry_number' => $new->national_registry_number,
            'birth_date' => $new->birth_date->format('Y-m-d 00:00:00'),
        ]);
    }
}
