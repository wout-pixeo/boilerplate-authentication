<?php

namespace Tests\Feature\User;

use Tests\TestCase;
use App\Services\Files\Upload;
use Illuminate\Http\UploadedFile;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use App\Domain\Media\Actions\UploadFile;
use App\Domain\Modules\Models\ModuleItem;
use App\Domain\Modules\Enums\FormFieldType;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::user());

        /** @var Module $this->module */
        $this->module = factory(Module::class)->create();
        $this->module->moduleItems()->save(factory(ModuleItem::class)->make(['user_id' => $this->user->id]));
        $this->moduleItem = $this->module->moduleItems()->first();
    }

    /** @test */
    public function it_can_view_a_form()
    {
        /** @var Form $form */
        $form = factory(Form::class)->state('fields')->create([
            'module_id' => $this->module->id,
        ]);

        $form->moduleItems()->save($this->moduleItem, ['form_audit_id' => $form->audits()->first()->id]);

        $this->actingAs($this->user)
            ->get(route('forms.show', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]))
            ->assertOk();
    }

    /** @test */
    public function it_can_update_a_form()
    {
        /** @var Form $form */
        $form = factory(Form::class)->state('fields')->create([
            'module_id' => $this->module->id,
        ]);

        $form->moduleItems()->save($this->moduleItem, ['form_audit_id' => $form->audits()->first()->id]);

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                collect($form->fields)->pluck('submit_value', '_cuid')->toArray()
            )
            ->assertRedirect();

        $this->assertEquals(
            collect($form->fields)->pluck('submit_value', '_cuid')->toArray(),
            $form->fresh()->moduleItems()->first()->pivot->data
        );
    }

    /** @test */
    public function it_validates_a_form()
    {
        /** @var Form $form */
        $form = factory(Form::class)->state('required_fields')->create([
            'module_id' => $this->module->id,
        ]);

        $form->moduleItems()->save($this->moduleItem, ['form_audit_id' => $form->audits()->first()->id]);

        $this->actingAs($this->user)
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                collect($form->fields)->pluck('submit_value', '_cuid')->toArray()
            )
            ->assertSessionHasErrors(collect($form->fields)->pluck('_cuid')->toArray());
        ;
    }

    /** @test */
    public function it_passes_validation()
    {
        /** @var Form $form */
        $form = $this->createForm([
            [
                'type' => FormFieldType::input()->getName(),
                '_cuid' =>  FormFieldType::input()->getValue(),
                'validation' => ['required'],
            ],
        ]);

        $this->actingAs($this->user)
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                [FormFieldType::input()->getValue() => 'Hello World']
            )
            ->assertSessionDoesntHaveErrors();

        $this->assertEquals(
            [FormFieldType::input()->getValue() => 'Hello World'],
            $form->fresh()->moduleItems()->first()->pivot->data
        );
    }

    /** @test */
    public function it_validates_repeatable_fields()
    {
        /** @var Form $form */
        $form = $this->createForm([
            [
                'type' => FormFieldType::input()->getName(),
                '_cuid' =>  FormFieldType::input()->getValue(),
                'validation' => ['required'],
                'can_repeat' => true,
                'min_repeat' => 2,
                'max_repeat' => 5,
            ],
        ]);

        $this->actingAs($this->user)
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                [FormFieldType::input()->getValue() => ['Hello World']]
            )
            ->assertSessionHasErrors();

        $this->actingAs($this->user)
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                [FormFieldType::input()->getValue() => ['Hello World', null]]
            )
            ->assertSessionHasErrors();

        $this->actingAs($this->user)
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                [FormFieldType::input()->getValue() => ['Hello World', 'Foo', 'Bar', 'Baz', 'Quux', 'Quuux']]
            )
            ->assertSessionHasErrors();

        $this->actingAs($this->user)
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                [FormFieldType::input()->getValue() => ['Hello World', 'Foo', 'Bar']]
            )
            ->assertSessionDoesntHaveErrors();

        $this->assertEquals(
            [FormFieldType::input()->getValue() => ['Hello World', 'Foo', 'Bar']],
            $form->fresh()->moduleItems()->first()->pivot->data
        );
    }

    /** @test */
    public function it_can_upload_a_file()
    {
        // Upload a fake file
        $file = (new UploadFile())->execute(new UploadedFile($this->getTestJpg(), 'foobar.jpg'));
        $form = $this->createForm([
            [
                'type' => FormFieldType::file()->getName(),
                '_cuid' => FormFieldType::file()->getValue(),
                'validation' => ['required'],
            ],
        ]);

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->put(
                route('forms.update', ['moduleItemId' => $this->moduleItem->id, 'id' => $form->id]),
                [FormFieldType::file()->getValue() => [$file->toArray()]]
            )
            ->assertSessionDoesntHaveErrors()
            ->assertRedirect();

        $this->assertEquals(
            [FormFieldType::file()->getValue() => [1]],
            $form->fresh()->moduleItems()->first()->pivot->data
        );
    }

    /** @test */
    public function it_can_submit_a_form()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_cant_see_forms_that_dont_belong_to_the_user()
    {
        $this->markTestIncomplete();
    }

    /**
     * @return Form
     */
    protected function createForm($fields = []): Form
    {
        /** @var Form $form */
        $form = factory(Form::class)->create([
            'module_id' => $this->module->id,
            'fields' => $fields,
        ]);

        $form->moduleItems()->save($this->moduleItem, ['form_audit_id' => $form->audits()->first()->id]);

        return $form;
    }
}
