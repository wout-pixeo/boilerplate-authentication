<?php

namespace Tests\Feature\User;

use Tests\TestCase;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Models\Module;
use App\Domain\Modules\Models\Question;
use App\Domain\Modules\Models\ModuleItem;
use App\Domain\Workflows\Models\Workflow;
use App\Domain\Modules\Models\FormModuleItem;
use App\Domain\Modules\Models\QuestionAnswer;
use App\Domain\Modules\Enums\ModuleItemStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Domain\Workflows\Models\WorkflowItemTemplate;

class ModuleItemsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpCity();
        $this->setupUser(\App\Domain\Users\Enums\Role::user());

        $this->module = factory(Module::class)->create();
        $this->workflow = tap(factory(Workflow::class)->make(), function ($model) {
            $model->city()->associate($this->city);
            $model->typeable()->associate($this->module);
            $model->save();
        });
    }

    /** @test */
    public function it_can_create()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_can_view_module_items()
    {
        $models = factory(ModuleItem::class, 5)->create([
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
        ]);

        $this->actingAs($this->user)
            ->get(route('modules.index', $this->module->slug))
            ->assertStatus(200)
            ->assertPropCount('models.data', 5)
            ->assertPropValue('models.data', function ($models) {
                $this->assertEquals(
                    ['id', 'user_id', 'module_id', 'status', 'created_at', 'updated_at', 'deleted_at'],
                    array_keys($models[0])
                );
            });
    }

    /** @test */
    public function it_can_create_a_module_item()
    {
        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('modules.store', ['module' => $this->module->slug]), [])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('module_items', [
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
            'status' => ModuleItemStatus::created()->getIndex(),
        ]);
    }

    /** @test */
    public function it_does_not_link_duplicate_forms()
    {
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);
        $answers = factory(Question::class, 2)
            ->create(['module_id' => $this->module->id])
            ->map(function (Question $question) use ($form) {
                $question->answers()->save(factory(QuestionAnswer::class)->state('default')->make());

                /** @var QuestionAnswer $answer */
                $answer = $question->answers()->first();
                $answer->forms()->sync($form);

                return [
                    'question_id' => $question->id,
                    'answer_id' => $question->answers()->first(['id'])->id,
                ];
            });

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('modules.store', ['module' => $this->module->slug]), [
                'answers' => $answers->toArray(),
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('module_items', [
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
            'status' => ModuleItemStatus::created()->getIndex(),
        ]);

        $this->assertDatabaseHas('form_module_item', ['form_id' => $form->id]);
        $this->assertCount(1, $form->moduleItems);
    }

    /** @test */
    public function it_adds_workflow_items_to_the_linked_form()
    {
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);
        $question = tap(factory(Question::class)->create(['module_id' => $this->module->id]), function (Question $question) use ($form) {
            $question->answers()->save(factory(QuestionAnswer::class)->state('default')->make());

            /** @var QuestionAnswer $answer */
            $answer = $question->answers()->first();
            $answer->forms()->sync($form);
        });

        $workflow = tap(factory(Workflow::class)->make(), function (Workflow $model) use ($form) {
            $model->city()->associate($this->city);
            $model->typeable()->associate($form);
            $model->save();
            $model->templates()->save(factory(WorkflowItemTemplate::class)->make([]));
        });

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('modules.store', ['module' => $this->module->slug]), [
                'answers' => [
                    'question_id' => $question->id,
                    'answer_id' => $question->answers()->first(['id'])->id,
                ],
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('module_items', [
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
            'status' => ModuleItemStatus::created()->getIndex(),
        ]);

        $this->assertDatabaseHas('form_module_item', ['form_id' => $form->id]);
        $this->assertDatabaseHas('workflow_items', [
            'typeable_type' => FormModuleItem::class,
        ]);
        $this->assertCount(1, $form->moduleItems);
    }

    /** @test */
    public function it_links_forms_and_makes_required_forms_take_priority()
    {
        $form = factory(Form::class)->create(['module_id' => $this->module->id]);
        $required = false;

        $questions = factory(Question::class, 2)
            ->create(['module_id' => $this->module->id])
            ->map(function (Question $question) use ($form, &$required) {
                $question->answers()->save(factory(QuestionAnswer::class)->state('default')->make());

                /** @var QuestionAnswer $answer */
                $answer = $question->answers()->first();

                $required = ! $required;

                $answer->forms()->sync([$form->id => ['required' => $required]]);

                return $question;
            });

        $this->actingAs($this->user)
            ->withoutExceptionHandling()
            ->post(route('modules.store', ['module' => $this->module->slug]), [
                'answers' => $questions->map(function ($question) {
                    return [
                        'question_id' => $question->id,
                        'answer_id' => $question->answers()->first(['id'])->id,
                    ];
                })->toArray(),
            ])
            ->assertStatus(302)
            ->assertSessionHas('success');

        $this->assertDatabaseHas('module_items', [
            'user_id' => $this->user->id,
            'module_id' => $this->module->id,
            'status' => ModuleItemStatus::created()->getIndex(),
        ]);

        $this->assertDatabaseHas('form_module_item', ['form_id' => $form->id, 'required' => true]);
        $this->assertCount(1, $form->moduleItems);
    }

    /** @test */
    public function it_executes_actions_when_module_item_is_created()
    {
        $this->markTestIncomplete();
    }

    /** @test */
    public function it_can_view_module_item_detail()
    {
        $this->markTestIncomplete();
    }
}
