<?php

use Illuminate\Database\Seeder;

class FileSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $path = database_path('sql/data_env_'.app()->environment().'.sql');
        \Illuminate\Support\Facades\DB::unprepared(file_get_contents($path));
    }
}
