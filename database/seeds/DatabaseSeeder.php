<?php

use App\Models\Country;
use App\Models\Location;
use App\Models\Department;
use App\Models\ThirdParty;
use Illuminate\Database\Seeder;
use App\Domain\Users\Models\User;
use App\Domain\Cities\Models\City;
use App\Domain\Events\Models\Club;
use Spatie\Permission\Models\Role;
use App\Domain\Cities\Models\Domain;
use App\Domain\Modules\Models\Question;
use App\Domain\Events\Models\ClubCategory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create countries
        $belgium = factory(Country::class)->create([
            'name' => 'België',
            'api' => true,
        ]);

        $holland = factory(Country::class)->create([
            'name' => 'Nederland',
            'api' => false,
        ]);

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        $rootUser = factory(User::class)->create([
            'first_name' => 'Koen',
            'last_name' => 'Mertens',
            'email' => 'root@example.com',
            'country_id' => $belgium->id,
        ]);

        $adminUser = factory(User::class)->create([
            'first_name' => 'Nick',
            'last_name' => 'Goris',
            'email' => 'admin@example.com',
            'country_id' => $belgium->id,
        ]);

        $organiserUser = factory(User::class)->create([
            'first_name' => 'Inge',
            'last_name' => 'Iemants',
            'email' => 'organiser@example.com',
            'country_id' => $belgium->id,
        ]);

        $rootRole = Role::create(['name' => \App\Domain\Users\Enums\Role::root()]);
        $rootUser->roles()->attach($rootRole->id);

        $adminRole = Role::create(['name' => \App\Domain\Users\Enums\Role::admin()]);
        $adminUser->roles()->attach($adminRole->id);

        $organiserRole = Role::create(['name' => \App\Domain\Users\Enums\Role::user()]);
        $organiserUser->roles()->attach($organiserRole->id);

        $city = factory(City::class)->create([
            'country_id' => $belgium->id,
        ]);

        factory(Domain::class)->create([
            'url' => 'https://test.flowlab.test',
            'default' => true,
            'city_id' => $city->id,
        ]);

        app()->instance('App\CurrentCity', $city);

        // Create workflow
        $module = factory(\App\Domain\Modules\Models\Module::class)->create();

//        factory(Department::class, 10)->create(['city_id' => $city->id]);
//        factory(ThirdParty::class, 10)->create(['city_id' => $city->id]);
//        factory(Location::class, 250)->create(['city_id' => $city->id]);
//        factory(ClubCategory::class, 10)->create(['city_id' => $city->id]);
//        factory(Club::class, 100)->create(['city_id' => $city->id]);
//        factory(Question::class, 10)->create(['city_id' => $city->id])->each(function ($question) {
//            $question->answers()->createMany([
//                ['value' => 'Nee', 'default' => true],
//                ['value' => 'Ja', 'default' => false],
//            ]);
//        });
    }
}
