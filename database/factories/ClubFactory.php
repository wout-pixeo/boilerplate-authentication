<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Events\Models\Club;

$factory->define(Club::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'email' => $faker->email,
        'street' => $faker->streetName,
        'number' => $faker->numberBetween(0, 100),
        'box' => $faker->numberBetween(0, 100),
        'postcode' => $faker->postcode,
        'city' => $faker->city,
        'country' => $faker->country,
        'phone' => $faker->phoneNumber,
        'url' => $faker->url,
    ];
});
