<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Country;
use Faker\Generator as Faker;
use App\Domain\Cities\Models\City;

$factory->define(City::class, function (Faker $faker) {
    $faker->locale = 'nl_BE';
    $faker->addProvider(new \Faker\Provider\nl_BE\Person($faker));
    $faker->addProvider(new \Faker\Provider\nl_BE\Address($faker));

    return [
        'name' => $faker->cityName(),
        'street' => $faker->streetName,
        'number' => $faker->numberBetween(),
        'box' => $faker->numberBetween(1, 10),
        'postcode' => $faker->postcode,
        'city' => $faker->cityName(),
        'country_id' => function () {
            return factory(Country::class)->create()->id;
        },
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'website' => $faker->url,
        'intro' => $faker->paragraph,
        'footer' => $faker->paragraph,
        'privacy_link' => $faker->url,
        'disclaimer_link' => $faker->url,
        'postmark_sender_signature_id' => 1,
        'postmark_sender_signature_email' => $faker->email,
        'postmark_sender_signature_name' => $faker->name,
        'postmark_sender_signature_reply_to' => $faker->email,
    ];
});
