<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Modules\Models\Question;
use App\Domain\Modules\Enums\QuestionType;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->sentence,
        'type' => QuestionType::radio()->getIndex(),
    ];
});
