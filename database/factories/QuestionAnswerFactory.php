<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Modules\Models\QuestionAnswer;

$factory->define(QuestionAnswer::class, function (Faker $faker) {
    return [
        'value' => $faker->name,
        'default' => false,
    ];
});

$factory->state(QuestionAnswer::class, 'default', [
    'default' => true,
]);
