<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ThirdParty;
use Faker\Generator as Faker;

$factory->define(ThirdParty::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ];
});
