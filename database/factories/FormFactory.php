<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Modules\Models\Form;
use App\Domain\Modules\Enums\FormFieldType;

$factory->define(Form::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'contact_name' => $faker->name,
        'contact_email' => $faker->email,
        'fields' => [],
    ];
});

$factory->state(Form::class, 'fields', [
    'fields' => collect(FormFieldType::toArray())->map(function ($type) {
        $enum = FormFieldType::make($type);

        switch (true) {
            case $enum->isEqual(FormFieldType::boolean()):
                $submitValue = null;
                break;
            case $enum->isEqual(FormFieldType::radio()):
            case $enum->isEqual(FormFieldType::checkbox()):
            case $enum->isEqual(FormFieldType::file()):
                $submitValue = [];
                break;
            default:
                $submitValue = null;
                break;
        }

        return [
            'title' => $enum->getName(),
            'type' => $enum->getName(),
            '_cuid' => str_random(32).'_'.$enum->getName(),
            // This is only for testing purposes. It will not be saved this way in
            // the actual database
            'submit_value' => $submitValue,
        ];
    })->toArray(),
]);

$factory->state(Form::class, 'required_fields', [
    'fields' => collect(FormFieldType::toArray())->map(function ($type) {
        $enum = FormFieldType::make($type);

        switch (true) {
            case $enum->isEqual(FormFieldType::boolean()):
                $submitValue = null;
                break;
            case $enum->isEqual(FormFieldType::radio()):
            case $enum->isEqual(FormFieldType::checkbox()):
            case $enum->isEqual(FormFieldType::file()):
                $submitValue = [];
                break;
            default:
                $submitValue = null;
                break;
        }

        return [
            'title' => $enum->getName(),
            'type' => $enum->getName(),
            '_cuid' => str_random(32).'_'.$enum->getName(),
            // This is only for testing purposes. It will not be saved this way in
            // the actual database
            'submit_value' => $submitValue,
            'validation' => ['required'],
        ];
    })->toArray(),
]);
