<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Modules\Models\ModuleItem;

$factory->define(ModuleItem::class, function (Faker $faker) {
    return [
        'status' => \App\Domain\Modules\Enums\ModuleItemStatus::created()->getIndex(),
    ];
});
