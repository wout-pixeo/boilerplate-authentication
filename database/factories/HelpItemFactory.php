<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Support\Models\HelpItem;

$factory->define(HelpItem::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'reference' => $faker->slug,
        'value' => $faker->sentence,
    ];
});
