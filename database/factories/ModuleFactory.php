<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Modules\Models\Module;

$factory->define(Module::class, function (Faker $faker) {
    return [
        'name' => 'Dossierstroom',
    ];
});
