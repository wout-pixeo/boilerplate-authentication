<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Cities\Models\Domain;

$factory->define(Domain::class, function (Faker $faker) {
    return [
        'url' => $faker->url,
        'default' => false,
    ];
});
