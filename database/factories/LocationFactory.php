<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Location;
use Faker\Generator as Faker;

$factory->define(Location::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'street' => $faker->streetName,
        'number' => $faker->numberBetween(),
        'box' => $faker->numberBetween(1, 10),
        'postcode' => $faker->postcode,
        'city' => $faker->city,
        'country' => $faker->country,
    ];
});
