<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Workflows\Enums\WorkflowItemType;
use App\Domain\Workflows\Models\WorkflowItemTemplate;

$factory->define(WorkflowItemTemplate::class, function (Faker $faker) {
    return [
        'name' => $faker->paragraph,
        'type' => WorkflowItemType::todo()->getName(),
    ];
});
