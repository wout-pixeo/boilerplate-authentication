<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Domain\Events\Models\ClubCategory;

$factory->define(ClubCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
